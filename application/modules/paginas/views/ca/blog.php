<div>[header]</div>
<div id="pageTitle">
			<div class="container">
				<!-- Breadcrumbs Block -->
				<div class="breadcrumbs">
					<ul class="breadcrumb">
						<li><a href="index.html">Home</a></li>
						<li class="active">Blog Posts</li>
					</ul>
				</div>
				<!-- //Breadcrumbs Block -->
				<h1>Blog <span class="color">Posts</span></h1>
			</div>
		</div>
		<div id="pageContent">
			<div class="container">
				<div class="row">
					<div class="col-md-9 column-center">
						[foreach:blog]
							<div class="blog-post">
								<div class="post-image">
									<a href="[link]">
										<img src="[foto]" alt="">
									</a>
								</div>
								<ul class="post-meta">
									<li class="author">Per <b><i>[user]</i></b></li>
									<li><i class="icon icon-clock"></i><span>[fecha]</span></li>									
								</ul>
								<h2 class="post-title">[titulo]</h2>
								<div class="post-teaser">
									<p>[texto]</p>
								</div>
								<a href="[link]" class="btn"><span>Veure més</span></a> 
							</div>
						[/foreach]
	
						<div id="postPreload"></div>
						<div id="moreLoader" class="more-loader">
							<img src="<?= base_url() ?>theme/theme/images/ajax-loader.gif" alt="">
						</div>
						<div class="divider"></div>
					</div>
					<div class="col-md-3 column-right">
						
						<div class="side-block">
							<h3>Post Categories</h3>
							<ul class="category-list">
								[foreach:categorias]
								<li><a href="[base_url]blog?blog_categorias_id=[id]">[blog_categorias_nombre]  <span>([cantidad])</span></a></li>
								[/foreach]
							</ul>
						</div>
						<div class="side-block">
							<h3>Popular tags</h3>
							<ul class="tags-list">
								[tags]								
							</ul>
						</div>
						<div class="side-block">
							<h3>Últimes Notícies</h3>
							[foreach:recientes]
							<div class="blog-post post-preview">
								<div class="post-image">
									<a href="[link]">
										<img src="[foto]" alt="">
									</a>
								</div>
								<ul class="post-meta">
									<li><i class="icon icon-clock"></i><span>[fecha]</span></li>									
								</ul>
								<h5 class="post-title">
									<a href="[link]">[titulo]</a></h5>
								<ul class="post-meta">
									<li class="author">per <b><i>[user]</i></b></li>
								</ul>
								<div class="post-teaser">
									<p>[texto]</p>
								</div>
							</div>
							[/foreach]
						</div>
					</div>
				</div>
			</div>
		</div>
<div>[footer]</div>