<!--Header-->
<header id="header" class="header header-container header-container-3 alt reveal">
	<!--Top Link-->
	<div class="top-links">
		<div class="container">
			<div class="row">
					<div class="col-md-10">
						<div class="top-link">
							<span class="address-top"><i class="fa fa-map-marker "></i>Room 1214  #187 Tay Son Building, Dong Da Distr Ha Noi, Vietnam</span>
							<span class="call-top"><i class="fa fa-phone"></i>Call us:  (0123) 456 7893</span>
						</div>
					</div>
					<div class="quick-access col-md-2 col-sm-2">								
						<div class="shopping-cart">
							<a href="#"><i class="fa fa-shopping-cart"></i></a>
						</div>
						<div class="search">
							<a href="#"><i class="fa fa-search"></i></a>
							<form id="search" method="post" name="">
								<div class="search-box">
									<input class="top-search" type="text" placeholder="Enter your keywords" name="search" title="Search">						
									<input class="sub-search" type="image" src="<?= base_url() ?>theme/theme/images/search.png" alt="Submit">
								</div>
							</form>										
						</div>
					</div>
			</div>
		</div>
	</div>
	<!--End Top Link-->
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-sm-3 col-xs-3 logo">
				<a href="index.html"><img src="<?= base_url() ?>theme/theme/images/logo.png" alt=""/></a>
			</div>
			<div class="col-md-10 nav-container">
				<nav class="megamenu collapse navbar-collapse bs-navbar-collapse navbar-right mainnav col-md-10" role="navigation">
					<ul class="nav-menu">
						<li class="selected active">
							<a href="#">Home</a>
							<ul class="child-nav dropdown-nav">
								<li><a href="index-v2.html">Home V2</a></li>
								<li><a href="index-onepage.html">One Page</a></li>
								<li><a href="index-layer.html">Layer Slider</a></li>
								<li><a href="index-shop.html">E-commerce</a></li>
								<li><a href="index-store.html">Store</a></li>
								<li><a href="index-sport-club.html">Sport Club</a></li>
								<li><a href="index-boxing.html">Boxing</a></li>
								<li><a href="index-white.html">Light version</a></li>
								<li><a href="coming-soon.html">Under Construction</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Header Option</a>
							<ul class="child-nav">
								<li><a href="index-header-v2.html">Header Style 1</a></li>
								<li><a href="index-header-v3.html">Header Style 2</a></li>
								<li><a href="index-header-v4.html">Header Style 3</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Page!</a>
							<ul class="child-nav">
								<li><a href="about-us.html">About us</a></li>
								<li><a href="pricing-table.html">Pricing table</a></li>
								<li><a href="our-class.html">Our classes</a></li>
								<li><a href="class-details.html">Class details</a></li>
								<li><a href="our-trainer.html">Our trainers</a></li>
								<li><a href="trainer-profile.html">Trainer profile</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Product</a>
							<ul class="child-nav">
								<li><a href="product-listing.html">Product listing</a></li>
								<li><a href="product-listing-row.html">Product listing row</a></li>
								<li><a href="product-detail.html">Product detail</a></li>
								<li><a href="product-cart.html">Product cart</a></li>
								<li><a href="product-check-out.html">Product checkout</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Event calendar</a>
							<ul class="child-nav">
								<li><a href="calendar-full-view.html">Calendar full view</a></li>
								<li><a href="event-listing.html">Event listing </a></li>
								<li><a href="event-listing-grid.html">Event listing grid</a></li>
								<li><a href="event-details.html">Event details</a></li>
							</ul>
						</li>
						<li>
							<a href="#">Blog</a>
							<ul class="child-nav">
								<li><a href="blog-listing.html">Blog listing</a></li>
								<li><a href="blog-details.html">Blog details</a></li>
							</ul>
						</li>
						<li><a href="contact-us.html">Contact us</a></li>
					</ul>
				</nav>							
			</div>							
			<button class="menu-button" id="open-button"></button>
		</div>
	</div>
</header>
<!--End Header-->