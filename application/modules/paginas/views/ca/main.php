<section  class="page cms-home header-option">
	<?php $this->load->view($this->theme.'_menu_mobile',array(),FALSE,'paginas'); ?>	
	<div class="content-wrapper">
		<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>	
		<!--Banner-->
		<section class="slide-container to-top">
			<div class="ms-fullscreen-template" id="slider1-wrapper">
				<!-- masterslider -->
				<div class="master-slider ms-skin-default" id="masterslider-index">							
					<div class="ms-slide slide-1" data-delay="0">
						<div class="slide-pattern"></div>
						<video data-autopause="false" data-mute="true" data-loop="true" data-fill-mode="fill">
							<source src="<?= base_url() ?>theme/theme/images/video/demo.mp4" type="video/mp4"/>									
						</video>  
						<h3 class="ms-layer hps-title1" style="left:101px"
							data-type="text"
							data-ease="easeOutExpo"
							data-delay="1000"
							data-duration="0"
							data-effect="skewleft(30,80)"
						>
							Athlete Fitness Club
						</h3>																												
						<h3 class="ms-layer hps-title3" style="left:95px"
							data-type="text"
							data-delay="1900"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,40,t)"
							data-ease="easeOutExpo"
						>
							Make You Be The Fighter
						</h3>
						
						<h3 class="ms-layer hps-title4" style="left:101px"
							data-type="text"
							data-delay="2500"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,18,t)"
							data-ease="easeOutExpo"
						>
							Try A Free Class
						</h3>
					</div>
					<div class="ms-slide slide-2" data-delay="0">
					   <div class="slide-pattern"></div>							  
						<img src="<?= base_url() ?>theme/theme/images/blank.gif" data-src="<?= base_url() ?>theme/theme/images/bg_1.jpg" alt="lorem ipsum dolor sit"/>
						<h3 class="ms-layer hps-title1" style="left:101px"
							data-type="text"
							data-ease="easeOutExpo"
							data-delay="1000"
							data-duration="0"
							data-effect="skewleft(30,80)">
							Athlete Fitness Club
						</h3>																												
						<h3 class="ms-layer hps-title3" style="left:95px"
							data-type="text"
							data-delay="1900"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,40,t)"
							data-ease="easeOutExpo"
						>
							Make You Be The Fighter
						</h3>
						
						<h3 class="ms-layer hps-title4" style="left:101px"
							data-type="text"
							data-delay="2500"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,18,t)"
							data-ease="easeOutExpo"
						>
							Try A Free Class
						</h3>
					</div>	
					
					<div class="ms-slide slide-3" data-delay="0">
					   <div class="slide-pattern"></div>							  
						<img src="<?= base_url() ?>theme/theme/images/blank.gif" data-src="<?= base_url() ?>theme/theme/images/bg-home-v2.jpg" alt="lorem ipsum dolor sit"/>
						<h3 class="ms-layer hps-title1" style="left:101px"
							data-type="text"
							data-ease="easeOutExpo"
							data-delay="1000"
							data-duration="0"
							data-effect="skewleft(30,80)"
						>
							Athlete Fitness Club
						</h3>																												
						<h3 class="ms-layer hps-title3" style="left:95px"
							data-type="text"
							data-delay="1900"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,40,t)"
							data-ease="easeOutExpo"
						>
							Make You Be The Fighter
						</h3>
						
						<h3 class="ms-layer hps-title4" style="left:101px"
							data-type="text"
							data-delay="2500"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,18,t)"
							data-ease="easeOutExpo"
						>
							Try A Free Class
						</h3>
					</div>							
				</div>
				<!-- end of masterslider -->
				<div class="to-bottom" id="to-bottom"><i class="fa fa-angle-down"></i></div>
			</div>
		</section>
		<!--End Banner-->										
		<div class="contents-main" id="contents-main">				
			<!--Collection-->
				<div class="collection">
					<div class="container">
						<div class="row">									
							<div class="collection-content">
								<div class="col-md-4 col-sm-12 col-xs-12 box">											
									<div class="women-content">
										<div class="masonry-small">
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/newproducts/images-3.png">
											</div>
											<div class="price-table-text">
												<h3>Collection</h3>
												<h2>For Man</h2>
												<div class="border-bottom"></div>
												<p>Phasellus molestie magna non est bibendum non venenatis nisl tempor lorem ipsum dolor.</p>
												<span>From <span> $59</span></span>
											</div>
										</div>
										<div class="masonry-lagar">
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/store/img-shop.jpg">
											</div>
											<div class="price-table-text">
												<div class="masonry-lagar-content">
													<div class="title-masory">
														<h3>2015</h3>
														<h2>Best Trending</h2>
														<div class="border-bottom"></div>
													</div>
													<div class="text-masony">
														<p>Phasellus molestie magna non est bibendum non venenatis nisl tempor lorem ipsum dolor.</p>
														<span>From <span> $59</span></span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 col-sm-12 col-xs-12 box">											
									<div class="man-content">
										<div class="masonry-lagar">
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/store/img-shop-2.jpg">
											</div>
											<div class="price-table-text">
												<div class="masonry-lagar-content">
													<div class="title-masory">
														<h3>2015</h3>
														<h2>Best Selling</h2>
														<div class="border-bottom"></div>
													</div>
													<div class="text-masony">
														<p>Phasellus molestie magna non est bibendum non venenatis nisl tempor lorem ipsum dolor.</p>
														<span>From <span> $59</span></span>
													</div>
												</div>
											</div>
										</div>
										<div class="masonry-small">
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/newproducts/images-2.png">
											</div>
											<div class="price-table-text">
												<h3>Collection</h3>
												<h2>For Ladies</h2>
												<div class="border-bottom"></div>
												<p>Phasellus molestie magna non est bibendum non venenatis nisl tempor lorem ipsum dolor.</p>
												<span>From <span> $59</span></span>
											</div>
										</div>												
									</div>
								</div>
								<div class="col-md-4 col-sm-12 col-xs-12 box">											
									<div class="collect-content">
										<div class="masonry-small">
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/newproducts/images-1.png">
											</div>
											<div class="price-table-text">
												<h3>Fitness</h3>
												<h2>Accessories</h2>
												<div class="border-bottom"></div>
												<p>Phasellus molestie magna non est bibendum non venenatis nisl tempor lorem ipsum dolor.</p>
												<span>From <span> $59</span></span>
											</div>
										</div>	
										<div class="masonry-lagar">													
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/store/img-shop-3.jpg">
											</div>
											<div class="price-table-text">
												<div class="masonry-lagar-content">
													<div class="title-masory">
														<h3>Famous</h3>
														<h2>Brands</h2>
														<div class="border-bottom"></div>
													</div>
													<div class="text-masony">
														<p>Phasellus molestie magna non est bibendum non venenatis nisl tempor lorem ipsum dolor.</p>
														<span>From <span> $59</span></span>
													</div>
												</div>
											</div>
										</div>																							
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<!--End Collection-->
			<!--Training-->
			<section class="training scroll-to">
				<!-- PARALLAX WINDOWS -->
				<div class="parallax-block-wrap-module auto-width">
					<div class="parallax-block bt-advance-parallax" id="parallax-block">
						
						<!-- PARALLAX CONTENT -->
						<div class="control-button">
							<div class="nav-wrap hidden">
								<div class="nav-wrap-in next">
									<span class="nav-next"></span>
								</div>
								<div class="nav-wrap-in prev">
									<span class="nav-prev"></span>
								</div>
							</div>
							<div style="display: none;" class="button-wrap">
								<span class="button close-btn"></span>
							</div>
						</div>
						<div class="parallax-background">
							<!--Textured and color-->
							<div class="parallax-background-overlay"></div>
							<img alt="" src="<?= base_url() ?>theme/theme/images/bg-position-3.1.jpg">										
						</div>
						
						<div class="parallax-block-content default-pos parallax-text parallax-v2">									
							<div class="container">
								<p class="strart-your">Start your</p>
								<h1 class="training-title">Training today</h1>
								<span class="button open-btn"><span>Play Now</span></span>
							</div>
						</div>
						
						<div class="overlay-loading"><span class="loading"><img src="<?= base_url() ?>theme/theme/images/loading-black.gif" alt=""/></span></div>    

						<!-- POPUP CONTENT -->
						<div  class="parallax-content-wrap">
							<div class="parallax-content hidden">
								<div class="parallax-content-in">
								</div>
								<div class="content-show-large">
									<div class="item-contain">
										<div class="video-inner">
											<!-- VIDEO TAG OR EMBED CODE -->
											<video data-fill-mode="fill" data-loop="true" data-autopause="false" loop>
												<source type="video/mp4" src="<?= base_url() ?>theme/theme/images/video/demo.mp4" />									
											</video> 	
										</div>
									</div>
									<div class="loading"><img src="<?= base_url() ?>theme/theme/images/loading-black.gif" alt=""/></div>
								</div>
							</div>
						</div>
						<!-- end POPUP CONTENT -->
											  
					</div>
				</div>	
			</section>
			<!--End Training-->
			
			<!--Services-->		
			<section class="services">
				<div class="container">
					<div class="row">
						<div class="title-page">
							<h4><span>Athlete</span> Services</h4>
						</div>
						<div class="sevices-main">
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="sevices-wapper">	
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-1.png" alt=""/>
										</div>
										<h4 class="services-title">Athlete</h4>
										<hr class="border-title">
										<h3 class="services-title">Sport Training</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla</p>
										</div>										
									</div>										
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="sevices-wapper">	
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-2.png" alt=""/>
										</div>
										<h4 class="services-title">Athlete</h4>
										<hr class="border-title">
										<h3 class="services-title">Sport Training</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla</p>
										</div>	
									</div>										
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="sevices-wapper">	
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-3.png" alt=""/>
										</div>
										<h4 class="services-title">Athlete</h4>
										<hr class="border-title">
										<h3 class="services-title">Sport Training</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla</p>
										</div>	
									</div>										
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="sevices-wapper">	
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-4.png" alt=""/>
										</div>
										<h4 class="services-title">Athlete</h4>
										<hr class="border-title">
										<h3 class="services-title">Sport Training</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla</p>
										</div>	
									</div>								
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="sevices-wapper">		
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-4.png" alt=""/>
										</div>
										<h4 class="services-title">Athlete</h4>
										<hr class="border-title">
										<h3 class="services-title">Sport Training</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla</p>
										</div>	
									</div>										
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="sevices-wapper">	
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-6.png" alt=""/>
										</div>
										<h4 class="services-title">Athlete</h4>
										<hr class="border-title">
										<h3 class="services-title">Sport Training</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla</p>
										</div>	
									</div>										
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="sevices-wapper">		
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-7.png" alt=""/>
										</div>
										<h4 class="services-title">Athlete</h4>
										<hr class="border-title">
										<h3 class="services-title">Sport Training</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla</p>
										</div>	
									</div>									
								</div>									
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="sevices-wapper">	
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-8.png" alt=""/>
										</div>
										<h4 class="services-title">Athlete</h4>
										<hr class="border-title">
										<h3 class="services-title">Sport Training</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla</p>
										</div>	
									</div>										
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--End Services-->	
			<!-- Athlete Facts -->
					<section class="facts image-bg-2 ">
						<div class="background-overlay"></div>
						<div class="container">
							<div class="row">
								<div class="facts-page">
									<div class="title-page title-facts">
										<h3 class="module-title-h3">Ethlete Facts</h3>
										<p>Working from home meant we could vary snack and coffee breaks, change our desks or view, goof off, drink on the job, even spend the day in pajamas, </p>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="facts-content">
											<div class="facts-icon"><i class="fa fa-bolt"></i></div>
											<div class="count">
												<span id="counter-1">1344</span>
												<div class="facts-border"></div>
											</div>
											<p class="facts-text">Club member</p>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="facts-content">
											<div class="facts-icon"><i class="fa fa-anchor"></i></div>
											<div class="count">
												<span id="counter-2">153</span>
												<div class="facts-border"></div>
											</div>
											<p class="facts-text">Club trainers</p>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="facts-content">
											<div class="facts-icon"><i class="fa fa-drupal"></i></div>
											<div class="count">
												<span id="counter-3">89</span>
												<div class="facts-border"></div>
											</div>
											<p class="facts-text">Fitness class</p>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="facts-content">
											<div class="facts-icon"><i class="fa fa-bomb "></i></div>
											<div class="count">
												<span id="counter-4">344</span>
												<div class="facts-border"></div>
											</div>
											<p class="facts-text">Club equipment</p>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</section>
					<!-- End Athlete Facts -->			
				</div>			
				<div id="class">
					<!-- Athlete Class -->
					<section id="classes" class="classes">
						<div class="classes-content">
							<div class="classes-wapper">
								<div class="title-page">
									<h3 class="module-title-h3">Athlete Classes</h3>
									<p>Working from home meant we could vary snack and coffee breaks, change our desks or view, goof off, drink on the job, even spend the day in pajamas, </p>
								</div>
							</div>
							<div id="filters" class="filters button-group container">							
								<button  data-filter="*" class="filter button is-checked">All Class</button>
								<button  data-filter=".aerobic" class="filter button">Sport</button>
								<button  data-filter=".fitness" class="filter button">Fitness</button>
								<button  data-filter=".sport" class="filter button">Yoga</button>
								<button  data-filter=".boxing" class="filter button">Boxing</button>									
								<button  data-filter=".dance" class="filter button">Dance Sport</button>									
								<button  data-filter=".belly" class="filter button">Belly Dance</button>									
							</div>
							<div class="class-our">				
							<div class="classes-athlete">
								<div class="classes-inner">
									<div id="filtering-demo">			
										<section id="our-class-main" class="isotope">
											<div class="mix dance element-item col-xs-12 col-sm-6 col-md-4" data-category="dance">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-6.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="our-class-details.html">
																		<span>Benefits of Boxing</span>
																	</a>
																</div>
																<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																<div class="box-details">
																	<a href="our-class-details.html">Details</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>
											<div class="mix sport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-1.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="our-class-details.html">
																		<span>Benefits of Boxing</span>
																	</a>
																</div>
																<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																<div class="box-details">
																	<a href="our-class-details.html">Details</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>												
											<div class="mix fitness sport element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-3.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="our-class-details.html">
																		<span>Benefits of Boxing</span>
																	</a>
																</div>
																<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																<div class="box-details">
																	<a href="our-class-details.html">Details</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>											
											<div class="mix fsport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="boxing">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-4.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="#">
																		<span>Benefits of Boxing</span>
																	</a>
																</div>
																<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																<div class="box-details">
																	<a href="our-class-details.html">Details</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>
											<div class="mix aerobic fitness element-item col-xs-12 col-sm-6 col-md-4" data-category="aerobic">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-5.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="">
																		<span>Benefits of Boxing</span>
																	</a>
																</div>
																<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																<div class="box-details">
																	<a href="our-class-details.html">Details</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>												
											<div class="mix fitness belly element-item col-xs-12 col-sm-6 col-md-4" data-category="belly">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-2.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="our-class-details.html">
																		<span>Benefits of Boxing</span>
																	</a>
																</div>
																<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																<div class="box-details">
																	<a href="our-class-details.html">Details</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>
											<div class="mix fitness sport element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-10.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="#">
																		<span>Benefits of Boxing</span>
																	</a>
																</div>
																<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																<div class="box-details">
																	<a href="#">Details</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>													
											<div class="mix sport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="boxing">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-8.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="#">
																		<span>Benefits of Boxing</span>
																	</a>
																</div>
																<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																<div class="box-details">
																	<a href="#">Details</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>
											<div class="mix aerobic fitness element-item col-xs-12 col-sm-6 col-md-4" data-category="aerobic">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-9.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="#">
																		<span>Benefits of Boxing</span>
																	</a>
																</div>
																<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																<div class="box-details">
																	<a href="#">Details</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>									
										</section>										
									</div>
								</div>
							</div>
							</div>																		
						</div>
					</section>
					<!-- End Athlete Class -->
			<!--News-->	
			<section class="news-page">
				<div class="container">
					<div class="row">							
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="news-cont">
								<div class="title-page">
									<h4>Athlete online</h4>
									<h3>Latest Online</h3>									
									<h3>Lessons</h3>									
								</div>
								<p class="news-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla.
								<a href="#">Click here to see all</a></p>
								<div class="news-wapper">
									<div class="col-md-4 col-sm-4 col-xs-12">
										<div class="img-news">
											<img src="<?= base_url() ?>theme/theme/images/img-1.png" alt=""/>
										</div>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<div class="details-news">
											<h6>Boxing</h6>
											<a href="#">Los Angeles Event</a>
											<p>Consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla.</p>
										</div>
									</div>
								</div>
								<div class="news-wapper">
									<div class="col-md-4 col-sm-4 col-xs-12">
										<div class="img-news">
											<img src="<?= base_url() ?>theme/theme/images/img-2.png" alt=""/>
										</div>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<div class="details-news">
											<h6>Boxing</h6>
											<a href="#">Los Angeles Event</a>
											<p>Consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla.</p>
										</div>
									</div>
								</div>
								<div class="news-wapper">
									<div class="col-md-4 col-sm-4 col-xs-12">
										<div class="img-news">
											<img src="<?= base_url() ?>theme/theme/images/img-2.png" alt=""/>
										</div>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<div class="details-news">
											<h6>Boxing</h6>
											<a href="#">Los Angeles Event</a>
											<p>Consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="news-cont">
							<div class="about-btm-left about-box">
									<div class="about-bottom-title">
										<h4>Facebook</h4>
									</div>
									<div class="working-hours">
										<p>Ac dictum risus sagittis id morbi posu justo eleifend libero ultricies asunt</p>
										<div class="time-hours">
											<div class="time-hours-item">Monday - Friday <span>08:00 - 16:00</span></div>
											<div class="time-hours-item">Saturday <span>09:30 - 15:30</span></div>
											<div class="time-hours-item">Sunday <span>09:30 - 15:30</span></div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<!-- Brands -->
				<div class="brands-sport">
					<div class="container">
						<div class="row">
							<div class="brands-content">
								<div class="prev">
									<button class="circle icon-wrap t-nav-prev">
										<i class="fa fa-arrow-left"></i>
									</button>
								</div>
								<div id="owl-popular" class="brands-owl owl-carousel col-md-12" data-nav="t-nav-" data-plugin-options='{"autoPlay":false,"autoHeight":true}'>
									<div class="logo-barnds">
										<a href="#"><img alt="" src="<?= base_url() ?>theme/theme/images/sport/brands-1.png"></a>
									</div>
									<div class="logo-barnds">
										<a href="#"><img alt="" src="<?= base_url() ?>theme/theme/images/sport/brands-2.png"></a>
									</div>
									<div class="logo-barnds">
										<a href="#"><img alt="" src="<?= base_url() ?>theme/theme/images/sport/brands-3.png"></a>
									</div>
									<div class="logo-barnds">
										<a href="#"><img alt="" src="<?= base_url() ?>theme/theme/images/sport/brands-4.png"></a>
									</div>
									<div class="logo-barnds">
										<a href="#"><img alt="" src="<?= base_url() ?>theme/theme/images/sport/brands-5.png"></a>
									</div>
									<div class="logo-barnds">
										<a href="#"><img alt="" src="<?= base_url() ?>theme/theme/images/sport/brands-6.png"></a>
									</div>
									<div class="logo-barnds">
										<a href="#"><img alt="" src="<?= base_url() ?>theme/theme/images/sport/brands-1.png"></a>
									</div>
									<div class="logo-barnds">
										<a href="#"><img alt="" src="<?= base_url() ?>theme/theme/images/sport/brands-2.png"></a>
									</div>
								</div>
								<div class="next">
									<button class="circle icon-wrap t-nav-prev">
										<i class="fa fa-arrow-right "></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			<!--End Brands -->						
		</div>
	</div>
</section>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/shortcode-frontend.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.mixitup.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/classie.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/parallax/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/parallax/jquery.transform2d.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/parallax/script.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/parallax/parallax.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/masterslider.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/banner.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/waypoints.js"></script>	
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/template.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/dropdown.js"></script>	
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/main.js"></script>