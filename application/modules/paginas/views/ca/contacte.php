<div>[header]</div>
<!-- Content  -->
		<div id="pageTitle">
			<div class="container">
				<!-- Breadcrumbs Block -->
				<div class="breadcrumbs">
					<ul class="breadcrumb">
						<li><a href="index.html">Inici</a></li>
						<li class="active">Contacte</li>
					</ul>
				</div>
				<!-- //Breadcrumbs Block -->
				<h1>Contacte</h1>
			</div>
		</div>
		<div id="pageContent">
			<!-- Block -->
			<div class="block offset-sm">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<h3>Truca: 691 802 758</h3>
						</div>
						<div class="col-md-8 editContent">Per qualsevol consulta, comanda, pressupost, urgències... pots contactar-nos a:</div>
					</div>
					<div class="row row-grey">
						<div class="col-sm-4">
							<div class="contact-info">
								<h5></h5>								
								<i class="icon icon-locate"></i>
								<span class=" editContent">C/ Girona, 134 Barcelona 

									<br> CP 08037
								</span>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="contact-info">
								<h5></h5>
								<i class="icon icon-clock"></i>
								<span class="editContent">Dill-Div 9:30h-13:30h/16:30h-20h  
<br>Dis 9:30h-13:30h</span>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="contact-info">
								<h5>Les nostres xarxes socials</h5>
								<div class="social-links">
									<ul>
										<li>
											<a class="icon icon-facebook-logo" href="#"></a>
										</li>
										<li>
											<a class="icon icon-twitter-logo" href="#"></a>
										</li>
										<li>
											<a class="icon icon-instagram-logo" href="#"></a>
										</li>
										<li>
											<a class="icon icon-google-plus-logo" href="#"></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<h2>Contacta amb nosaltres</h2>
					<form id="contactform" class="contact-form" name="contactform" method="post" novalidate>
						<div id="success" class="successform">
							<p>El teu missatge s'ha enviat!</p>
						</div>
						<div id="error" class="errorform">
							<p>Algo ha fallat, intenta-ho un altre cop siusplau.</p>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="input-wrapper">
									<input type="text" class="input-custom input-full" name="nombre" placeholder="Nom i cognom">
								</div>
								<div class="input-wrapper">
									<input type="text" class="input-custom input-full" name="telefono" placeholder="Tèlefon">
								</div>
								<div class="input-wrapper">
									<input type="text" class="input-custom input-full" name="email" placeholder="E-mail">
								</div>
							</div>
							<div class="col-sm-6">
								<textarea class="textarea-custom input-full" name="message" placeholder="Missatge"></textarea>
							</div>
						</div>
						<div class="divider-sm"></div>
						<input type="checkbox" name="politicas" value="1"> Accepto les <a href="[base_url]p/aviso-legal">polítiques de privacitat</a><br/><br/>
						<button type="submit" id="submit-contact" class="btn"><i class="icon icon-lightning"></i><span>Enviar</span></button>
					</form>
				</div>
			</div>
			<!-- //Block -->
		</div>
		<!-- // Content  -->
<div>[footer]</div>