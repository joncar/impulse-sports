<!--Menu Mobile-->
<div class="menu-wrap">
	<div class="main-menu">
		<h4 class="title-menu">Main menu</h4>
		<button class="close-button" id="close-button"><i class="fa fa-times"></i></button>
	</div>
	<ul class="nav-menu">
		<li class="dropdown selected active">
			<a href="#" data-toggle="dropdown">Home<i class="icon-arrow"></i></a>
			<ul class="child-nav dropdown-nav">
				<li><a href="index-v2.html">Home V2</a></li>
				<li><a href="index-onepage.html">One Page</a></li>
				<li><a href="index-layer.html">Layer Slider</a></li>
				<li><a href="index-shop.html">E-commerce</a></li>
				<li><a href="index-store.html">Store</a></li>
				<li><a href="index-sport-club.html">Sport Club</a></li>
				<li><a href="index-boxing.html">Boxing</a></li>
				<li><a href="index-white.html">Light version</a></li>
				<li><a href="coming-soon.html">Under Construction</a></li>
			</ul>
		</li>
		
		<li><a href="contact-us.html">Contact us</a></li>
	</ul>
</div>
<!--Menu Mobile-->