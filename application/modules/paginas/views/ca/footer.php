<!--Footer-->
		<footer class="page-footer">
			<section>
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12 infomation">
							<div class="copy-right">
								<div class="footer-right">
									<div class="line1">Copyright &copy; 2014<a href="#"> Athlete Fitness</a></div>
									<div class="line2">Designed and development by InwaveThemes</div>
								</div>
							</div>
							<div class="social_icon">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-google-plus"></i></a>
								<a href="#"><i class="fa fa-youtube"></i></a>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 location">							
							<div class="footer-title">
								<h4>Our location</h4>			
							</div>
							<div class="address">
								<p>Room 1214&nbsp; #187 Tay Son Building, Dong Da Distr<br>Ha Noi, Vietnam<br>Phone : +84 4 22414757<br>Email : inwavethemes@gmail.com</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 send-mail">							
							<div class="footer-title">
								<h4>Keep in touch</h4>			
							</div>
							<form name="" method="post"  id="send-mail">												
								<div class="info">Lorem ipsum dolor sit amet, consectetur adi sollicitud ante tellus ornare mi, et mollis</div>						
								<div class="email">
									<input type="text" title="E-mail" name="user[email]" class="inputbox" placeholder="Your email" >
									<button class="button" title="Submit" type="submit"><i class="fa fa-arrow-right"></i></button>
								</div>
							</form>
						</div>						
					</div>
				</div>
			</section>
		</footer>
		<!--End Footer-->
		
		<!--To Top-->
		<div id="copyright">
			<div class="container">

				<div class="back-to-top"><a title="BACK_TO_TOP" href="#top"><i class="fa fa-chevron-up"></i></a></div>

				<div class="clrDiv"></div>
			</div>
		</div>
		<!--End To Top-->
	