<section  class="page page-category header-option">
	<?php $this->load->view($this->theme.'_menu_mobile',array(),FALSE,'paginas'); ?>
	<div class="content-wrapper">
		<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
		
		<!--Banner-->
		<section class="page-heading">
			<div class="title-slide">
				<div class="container">
					<div class="banner-content slide-container">
						<div class="page-title">
							<h3>Campus de verano</h3>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--End Banner-->
		<div class="page-content">
			<!-- Breadcrumbs -->
			<div class="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<ul>
								<li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
								<li><span>//</span></li>
								<li class="category-1"><a href="#">Servicios</a></li>
								<li><span>//</span></li>
								<li class="category-2">Campus de verano</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- End Breadcrumbs -->
			<!-- Main Content -->
			<div class="main-content class-detail">
				<div class="container">
					<div class="row p-b-45">
						<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
							<!-- Banner Content -->
							<section class="banner-details">
								<div id="carousel" class="carousel slide" data-ride="carousel">
									<!-- Wrapper for slides -->
									<div class="carousel-inner" role="listbox">
										<div class="item active">
											<img src="<?= base_url() ?>theme/theme/images/bg-blog-2.jpg" alt=""/>
										</div>
										
										<div class="item">
											<img src="<?= base_url() ?>theme/theme/images/bg-blog.jpg" alt=""/>
										</div>
										
										<div class="item">
											<img src="<?= base_url() ?>theme/theme/images/bg-blog-1.jpg" alt=""/>
										</div>
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
										<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#carousel" role="button" data-slide="next">
										<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div>
							</section>
							<!-- End Banner Content -->
							
							
							
							<!--  Desc -->
							<div class="content-page">
								<section class="class-details">
									<div class="details-desc">
										<div class="details-desc-title">
											<h5>En Barcelona</h5>
											<div class="rating">
												<span><i class="fa fa-star"></i></span>
												<span><i class="fa fa-star"></i></span>
												<span><i class="fa fa-star"></i></span>
												<span><i class="fa fa-star"></i></span>
												<span><i class="fa fa-star-half-o"></i></span>
												<span class="votes">234 votes</span>
											</div>
										</div>
										<div class="details-desc-full">
											<p>
												Este próximo mes de julio, llevaremos a cabo nuestra 11ª edición del campus de porteros Impulse Sport.
												Como cada verano, tenemos nuevas ilusiones y expectativas puestas en esta nueva edición del campus de verano, para que al igual que en las ediciones anteriores, puedas disfrutar y mejorar realizando aquello que más te agrada, jugar y entrenar como portero de fútbol.
											</p>
											<p>
												En nuestro campus vas a entrenar de forma específica las diferentes habilidades técnicas, tácticas y coordinativas esenciales para nuestra demarcación. Nuestra intención es enseñarte diferentes conceptos y fundamentos propios de la demarcación del portero para que, durante la siguiente temporada, puedas utilizarlos y te ayuden a optimizar tu rendimiento en la portería.
												Además el campus de porteros es una excelente oportunidad para convivir con otros porteros y de hacer nuevas amistades, todo ello en un entorno natural y clima de montaña idóneo para la práctica del fútbol.
											</p>
											<p><strong>Fechas: del domingo 7 de julio al sábado 13 de julio.
												<br>Lugar: Ripoll (Osona – Cataluña).
												<br>Aforo máximo: 30 porteros. </strong>
										    </p>
										</div>
									</div>
							
									<div class="comments-title">
										<div class="caveats-title">
											<h5>Programa</h5>
										</div>
										<div class="caveats-full">
											<p>
												El programa de entrenamiento del Campus Impulse Sport, durante esta semana, contemplará el trabajo intenso de las acciones específicas más fundamentales y básicas para el portero de fútbol como: la parada, el blocaje, el desvío, el despeje, la salida, la entrada, la colocación y la dirección de los defensas.
											</p>
											<p>
												Todas y cada una de estas acciones se trabajarán a través de las nuevas corrientes metodológicas utilizadas con los porteros del más alto nivel, enfatizando aspectos como el desarrollo de las diferentes habilidades coordinativas y la toma de decisiones en la resolución de las diferentes situaciones de juego.
											</p>
											<p>
												De esta manera, nuestros principales objetivos son:
												<br>• Aprender y mejorar las diferentes acciones específicas del portero de fútbol y sus fundamentos.
												<br>• Convivir con otros porteros y hacer nuevas amistades dentro de un grupo tan limitado y especial como el nuestro.
												<br>• Disfrutar del fútbol y de nuestra demarcación tan específica como desconocida para muchos.
												<br>• Aprender diferentes valores como el respeto, la colaboración, la competición, el sacrifico, el espíritu de autosuperación, el fair-play, el orden, etc.
											</p>
										</div>
										
										<div class="caveats-content">
											<p>
												"You can design and create, and build the most wonderful place in the world. But it takes people to make the dream a reality."
											</p>
										</div>
									</div>
							
									<div class="share">
										<div class="share-title">
											<h5>Share This Class</h5>
										</div>
										<div class="social-icon">
											<a href="#"><i class="fa fa-facebook"></i></a>
											<a href="#"><i class="fa fa-twitter"></i></a>
											<a href="#"><i class="fa fa-google-plus"></i></a>
											<a href="#"><i class="fa fa-youtube"></i></a>
											<a class="in" href="#"><i class="fa fa-linkedin"></i></a>
										</div>
									</div>
							
									<div class="comments-title">
										<div class="caveats-title">
											<h5>Equipación</h5>
										</div>
										<div class="caveats-full">
											<p>
												En esta edición del campus te regalaremos:
												<br>          • 1 camiseta Impulse Sport
												<br>    • 1 pantalón corto Impulse Sport
												
												<br>¡Además de otros premios y sorpresas!
												
											</p>
											<p>
												<strong>¿Qué debes traer?</strong>
												<ul style="margin:0; padding:0; color: #9e9e9e;">
													<li>• 2 chándales (pantalón y sudadera) que abriguen</li>
													<li>• Varios juegos de ropa de entrenamiento (pantalón y camiseta)</li>
													<li>• Botas de fútbol y zapatillas deportivas (“bambas”)</li>
													<li>• Guantes de portero</li>
													<li>• Ropa de calle</li>
													<li>• Bañador</li>
													<li>• Toalla de piscina</li>
													<li>• Chancletas</li>
													<li>• Neceser con tus cosas personales, ropa interior y protector solar (mínimo factor 30) para cuerpo y labios.</li>
												</ul>
											</p>
										</div>
										
										<div class="caveats-content">
											<p>
												Estás autorizado a traer móvil pero deberás tener en cuenta que solo podrás usarlo de 14:30 a 15:00 y de 21:30 a 22:00
											</p>
											<p>
												Tus padres también pueden llamarte al teléfono 616 51 31 26
											</p>
										</div>
									</div>
							
								</section>
								<div class="price-tb">
									<div class="col-md-12 price-table-content">
										<div class="price-table-img">
											<img alt="" src="<?= base_url() ?>theme/theme/images/newproducts/images-1.png">
										</div>
										<div class="price-table-text">
											<h3>La estructura del campus, en el día a día, es la siguiente:</h3>
											<h2>HORARIO</h2>
											<div class="border-bottom"></div>
											<p>* Este horario puede verse afectado en función de las visitas de profesionales que acudan al campus a dar alguna charla.
												
											Otras actividades: práctica de otros deportes (volei, fútbol-sala, baloncesto, ping-pong, etc.), piscina, cine para todos, charlas teóricas de formación y visualización de video.</p>
											<!--
											<div class="price">
													<span>$59</span>
											</div>
											-->
											<!-- <a href="#"><span>Join Now</span></a> -->
										</div>
									</div>
									<div class="col-md-12 price-list">
										<div class="price-table-1">
											<ul>
												<li class="icon"><i class="fa fa-calendar-o"></i></li>
												<li>Entry date: Mon - Fri</li>
											</ul>
											<ul>
												<li class="icon"><i class="fa fa-clock-o"></i></li>
												<li>Entry time: 8A.M - 8P.M</li>
											</ul>
											<ul>
												<li class="icon"><i class="fa fa-leaf"></i></li>
												<li>Nutrition Advicer: Yes</li>
											</ul>
										</div>
										<!--
										<div class="plan">
												<a href="#">Purchase Plan</a>
										</div>
										-->
									</div>
								</div>
							
								<section id="classes" class="classes">
									<div class="classes-content">
										<div class="classes-wapper">
											<div class="title-page">
												<h3 class="module-title-h3">Nuestros Campus</h3>
												<p>Aquí tienes un pequeño resumen de los Campus de Porteros que Impulse Sport ha hecho hasta el día de hoy </p>
											</div>
										</div>
										<div id="filters" class="filters button-group">
											<button  data-filter="*" class="filter button is-checked">Todos</button>
											<button  data-filter=".aerobic" class="filter button">Verano 2008</button>
											<button  data-filter=".fitness" class="filter button">Verano 2009</button>
											<button  data-filter=".sport" class="filter button">Verano 2010</button>
											<button  data-filter=".boxing" class="filter button">Navidad 2011</button>
											<button  data-filter=".dance" class="filter button">Verano 2012</button>
											<button  data-filter=".belly" class="filter button">Navidad 2012</button>
										</div>
										<div class="class-our">
											<div class="classes-athlete">
												<div class="classes-inner">
													<div id="filtering-demo">
														<section id="our-class-main" class="isotope">
															<div class="mix dance element-item col-xs-12 col-sm-6 col-md-4" data-category="dance">
																<div class="box-inner">
																	<img src="<?= base_url() ?>theme/theme/images/onepage/classes-6.jpg" alt=""/>
																	<div class="box-content">
																		<div class="table">
																			<div class="box-cell">
																				<div class="title">
																					<a href="our-class-details.html">
																						<span>Instalaciones Ripoll</span>
																					</a>
																				</div>
																				<div class="box-text"></div>
																				<div class="box-details">
																					<a href="our-class-details.html">Ver Galería</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="mix sport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
																<div class="box-inner">
																	<img src="<?= base_url() ?>theme/theme/images/onepage/classes-1.jpg" alt=""/>
																	<div class="box-content">
																		<div class="table">
																			<div class="box-cell">
																				<div class="title">
																					<a href="our-class-details.html">
																						<span>Ripoll 2014</span>
																					</a>
																				</div>
																				<div class="box-text"></div>
																				<div class="box-details">
																					<a href="our-class-details.html">Ver Galería</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="mix fitness sport element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
																<div class="box-inner">
																	<img src="<?= base_url() ?>theme/theme/images/onepage/classes-3.jpg" alt=""/>
																	<div class="box-content">
																		<div class="table">
																			<div class="box-cell">
																				<div class="title">
																					<a href="our-class-details.html">
																						<span>Barcelona 2014</span>
																					</a>
																				</div>
																				<div class="box-text"></div>
																				<div class="box-details">
																					<a href="our-class-details.html">Ver Galería</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="mix fsport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="boxing">
																<div class="box-inner">
																	<img src="<?= base_url() ?>theme/theme/images/onepage/classes-4.jpg" alt=""/>
																	<div class="box-content">
																		<div class="table">
																			<div class="box-cell">
																				<div class="title">
																					<a href="#">
																						<span>Ripoll 2016</span>
																					</a>
																				</div>
																				<div class="box-text"></div>
																				<div class="box-details">
																					<a href="our-class-details.html">Ver Galería</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="mix aerobic fitness element-item col-xs-12 col-sm-6 col-md-4" data-category="aerobic">
																<div class="box-inner">
																	<img src="<?= base_url() ?>theme/theme/images/onepage/classes-5.jpg" alt=""/>
																	<div class="box-content">
																		<div class="table">
																			<div class="box-cell">
																				<div class="title">
																					<a href="">
																						<span>Barcelona 2016</span>
																					</a>
																				</div>
																				<div class="box-text"></div>
																				<div class="box-details">
																					<a href="our-class-details.html">Ver Galería</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="mix fitness belly element-item col-xs-12 col-sm-6 col-md-4" data-category="belly">
																<div class="box-inner">
																	<img src="<?= base_url() ?>theme/theme/images/onepage/classes-2.jpg" alt=""/>
																	<div class="box-content">
																		<div class="table">
																			<div class="box-cell">
																				<div class="title">
																					<a href="our-class-details.html">
																						<span>Ripoll 2017</span>
																					</a>
																				</div>
																				<div class="box-text"></div>
																				<div class="box-details">
																					<a href="our-class-details.html">Ver Galería</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="mix fitness sport element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
																<div class="box-inner">
																	<img src="<?= base_url() ?>theme/theme/images/onepage/classes-10.jpg" alt=""/>
																	<div class="box-content">
																		<div class="table">
																			<div class="box-cell">
																				<div class="title">
																					<a href="#">
																						<span>Barcelona 2017</span>
																					</a>
																				</div>
																				<div class="box-text"></div>
																				<div class="box-details">
																					<a href="#">Ver Galería</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="mix sport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="boxing">
																<div class="box-inner">
																	<img src="<?= base_url() ?>theme/theme/images/onepage/classes-8.jpg" alt=""/>
																	<div class="box-content">
																		<div class="table">
																			<div class="box-cell">
																				<div class="title">
																					<a href="#">
																						<span>Ripoll 2018</span>
																					</a>
																				</div>
																				<div class="box-text"></div>
																				<div class="box-details">
																					<a href="#">Ver Galería</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="mix aerobic fitness element-item col-xs-12 col-sm-6 col-md-4" data-category="aerobic">
																<div class="box-inner">
																	<img src="<?= base_url() ?>theme/theme/images/onepage/classes-9.jpg" alt=""/>
																	<div class="box-content">
																		<div class="table">
																			<div class="box-cell">
																				<div class="title">
																					<a href="#">
																						<span>Barcelona 2018</span>
																					</a>
																				</div>
																				<div class="box-text"></div>
																				<div class="box-details">
																					<a href="#">Ver Galería</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</section>
													</div>
												</div>
											</div>
										</div>
									</div>
								</section>
								<!-- End Athlete Class -->
								
								<!-- End Desc -->
								<div class="contact-map" style="margin-top:60px;">
									<div class="map-frame-event" style="position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-style" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;"><div tabindex="0" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default;"><div style="z-index: 1; position: absolute; left: 50%; top: 50%; width: 100%; transform: translate(0px, 0px);"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; z-index: 989; transform: matrix(1, 0, 0, 1, -149, -93);"><div style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: -256px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: -256px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 0px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 256px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 256px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 256px; top: 256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 0px; top: 256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: -256px; top: 256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: -512px; top: 256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: -512px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: -512px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 512px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 512px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 512px; top: 256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="width: 108px; height: 53px; overflow: hidden; position: absolute; left: -110px; top: -62px; z-index: -9;"><img alt="" src="images/pin.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; width: 108px; height: 53px; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div style="position: absolute; z-index: 989; transform: matrix(1, 0, 0, 1, -149, -93);"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: -256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: -256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: -256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: 256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: 256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -512px; top: 256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -512px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -512px; top: -256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: -256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: 256px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; z-index: 989; transform: matrix(1, 0, 0, 1, -149, -93);"><div style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i603!3i770!4i256!2m3!1e0!2sm!3i444150302!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=76060" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -256px; top: 0px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i602!3i770!4i256!2m3!1e0!2sm!3i444150302!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=58906" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -256px; top: -256px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i602!3i769!4i256!2m3!1e0!2sm!3i444150302!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=92161" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 0px; top: -256px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i603!3i769!4i256!2m3!1e0!2sm!3i444150302!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=109315" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 256px; top: -256px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i604!3i769!4i256!2m3!1e0!2sm!3i444150302!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=126469" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 256px; top: 0px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i604!3i770!4i256!2m3!1e0!2sm!3i444150302!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=93214" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 256px; top: 256px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i604!3i771!4i256!2m3!1e0!2sm!3i444150266!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=92284" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 0px; top: 256px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i603!3i771!4i256!2m3!1e0!2sm!3i444150302!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=26127" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -256px; top: 256px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i602!3i771!4i256!2m3!1e0!2sm!3i444150302!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=8973" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -512px; top: 256px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i601!3i771!4i256!2m3!1e0!2sm!3i444150302!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=122890" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -512px; top: 0px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i601!3i770!4i256!2m3!1e0!2sm!3i444150302!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=41752" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -512px; top: -256px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i601!3i769!4i256!2m3!1e0!2sm!3i444150302!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=75007" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 512px; top: -256px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i605!3i769!4i256!2m3!1e0!2sm!3i444150278!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=90644" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 512px; top: 0px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i605!3i770!4i256!2m3!1e0!2sm!3i444150278!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=57389" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 512px; top: 256px; width: 256px; height: 256px; transition: opacity 200ms linear;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i11!2i605!3i771!4i256!2m3!1e0!2sm!3i444150218!2m3!1e2!6m1!3e5!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi02MHxwLmw6LTYw!4e0!23i1301875&amp;token=53014" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="z-index: 2; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; transition-duration: 0.8s; opacity: 0;"><p class="gm-style-pbt">Mantén pulsada la tecla ⌘ mientras te desplazas para acercar o alejar el mapa</p></div><div style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px;"><div style="z-index: 4; position: absolute; left: 50%; top: 50%; width: 100%; transform: translate(0px, 0px);"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"><div title="Athlete!" style="width: 108px; height: 53px; overflow: hidden; position: absolute; opacity: 0; cursor: pointer; left: -110px; top: -62px; z-index: -9;"><img alt="" src="images/pin.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; width: 108px; height: 53px; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div></div><iframe aria-hidden="true" frameborder="0" src="about:blank" style="z-index: -1; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; border: none;"></iframe><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" rel="noopener" href="https://maps.google.com/maps?ll=40.665313,-73.901698&amp;z=11&amp;t=m&amp;hl=es-ES&amp;gl=US&amp;mapclient=apiv3" title="Open this area in Google Maps (opens a new window)" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/google_white5.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-sizing: border-box; -webkit-box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 300px; height: 180px; position: absolute; left: 274px; top: 102px;"><div style="padding: 0px 0px 10px; font-size: 16px;">Datos de mapas</div><div style="font-size: 13px;">Datos de mapas ©2018 Google</div><button draggable="false" title="Close" aria-label="Close" type="button" class="gm-ui-hover-effect" style="background-image: none; display: block; border: 0px; margin: 0px; padding: 0px; position: absolute; cursor: pointer; -webkit-user-select: none; top: 0px; right: 0px; width: 37px; height: 37px; background-position: initial initial; background-repeat: initial initial;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2224px%22%20height%3D%2224px%22%20viewBox%3D%220%200%2024%2024%22%20fill%3D%22%23000000%22%3E%0A%20%20%20%20%3Cpath%20d%3D%22M19%206.41L17.59%205%2012%2010.59%206.41%205%205%206.41%2010.59%2012%205%2017.59%206.41%2019%2012%2013.41%2017.59%2019%2019%2017.59%2013.41%2012z%22%2F%3E%0A%20%20%20%20%3Cpath%20d%3D%22M0%200h24v24H0z%22%20fill%3D%22none%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="pointer-events: none; display: block; width: 13px; height: 13px; margin: 12px;"></button></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 243px; bottom: 0px; width: 165px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="text-decoration: none; cursor: pointer; display: none;">Datos de mapas</a><span>Datos de mapas ©2018 Google</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Datos de mapas ©2018 Google</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; -webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 149px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/es-ES_US/help/terms_maps.html" target="_blank" rel="noopener" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Términos de uso</a></div></div><button draggable="false" title="Cambiar a la vista en pantalla completa" aria-label="Cambiar a la vista en pantalla completa" type="button" class="gm-control-active gm-fullscreen-control" style="background-image: none; background-color: rgb(255, 255, 255); border: 0px; margin: 10px; padding: 0px; position: absolute; cursor: pointer; -webkit-user-select: none; border-top-left-radius: 2px; border-top-right-radius: 2px; border-bottom-right-radius: 2px; border-bottom-left-radius: 2px; height: 40px; width: 40px; -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; overflow: hidden; top: 0px; right: 0px; background-position: initial initial; background-repeat: initial initial;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%20018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23666%22%20d%3D%22M0%2C0v2v4h2V2h4V0H2H0z%20M16%2C0h-4v2h4v4h2V2V0H16z%20M16%2C16h-4v2h4h2v-2v-4h-2V16z%20M2%2C12H0v4v2h2h4v-2H2V12z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px; margin: 11px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23333%22%20d%3D%22M0%2C0v2v4h2V2h4V0H2H0z%20M16%2C0h-4v2h4v4h2V2V0H16z%20M16%2C16h-4v2h4h2v-2v-4h-2V16z%20M2%2C12H0v4v2h2h4v-2H2V12z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px; margin: 11px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23111%22%20d%3D%22M0%2C0v2v4h2V2h4V0H2H0z%20M16%2C0h-4v2h4v4h2V2V0H16z%20M16%2C16h-4v2h4h2v-2v-4h-2V16z%20M2%2C12H0v4v2h2h4v-2H2V12z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px; margin: 11px;"></button><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_blank" rel="noopener" title="Informar a Google acerca de errores en las imágenes o en el mapa de carreteras" href="https://www.google.com/maps/@40.6653126,-73.9016981,11z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Notificar un problema de Maps</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="40" controlheight="113" style="margin: 10px; -webkit-user-select: none; position: absolute; bottom: 127px; right: 40px;"><div class="gmnoprint" controlwidth="40" controlheight="81" style="position: absolute; left: 0px; top: 32px;"><div draggable="false" style="-webkit-user-select: none; -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-top-left-radius: 2px; border-top-right-radius: 2px; border-bottom-right-radius: 2px; border-bottom-left-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 40px; height: 81px;"><button draggable="false" title="Acerca la imagen" aria-label="Acerca la imagen" type="button" class="gm-control-active" style="background-image: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; -webkit-user-select: none; overflow: hidden; width: 40px; height: 40px; top: 0px; left: 0px; background-position: initial initial; background-repeat: initial initial;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpolygon%20fill%3D%22%23666%22%20points%3D%2218%2C7%2011%2C7%2011%2C0%207%2C0%207%2C7%200%2C7%200%2C11%207%2C11%207%2C18%2011%2C18%2011%2C11%2018%2C11%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px; margin: 9px 11px 13px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpolygon%20fill%3D%22%23333%22%20points%3D%2218%2C7%2011%2C7%2011%2C0%207%2C0%207%2C7%200%2C7%200%2C11%207%2C11%207%2C18%2011%2C18%2011%2C11%2018%2C11%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px; margin: 9px 11px 13px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpolygon%20fill%3D%22%23111%22%20points%3D%2218%2C7%2011%2C7%2011%2C0%207%2C0%207%2C7%200%2C7%200%2C11%207%2C11%207%2C18%2011%2C18%2011%2C11%2018%2C11%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px; margin: 9px 11px 13px;"></button><div style="position: relative; overflow: hidden; width: 30px; height: 1px; margin: 0px 5px; background-color: rgb(230, 230, 230); top: 0px;"></div><button draggable="false" title="Aleja la imagen" aria-label="Aleja la imagen" type="button" class="gm-control-active" style="background-image: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; -webkit-user-select: none; overflow: hidden; width: 40px; height: 40px; top: 0px; left: 0px; background-position: initial initial; background-repeat: initial initial;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23666%22%20d%3D%22M0%2C7h18v4H0V7z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px; margin: 13px 11px 9px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23333%22%20d%3D%22M0%2C7h18v4H0V7z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px; margin: 13px 11px 9px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23111%22%20d%3D%22M0%2C7h18v4H0V7z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px; margin: 13px 11px 9px;"></button></div></div><div style="position: absolute; left: 20px; top: 0px;"></div><div class="gmnoprint" controlwidth="40" controlheight="40" style="display: none; position: absolute;"><div style="width: 40px; height: 40px;"><button draggable="false" title="Rotate map 90 degrees" aria-label="Rotate map 90 degrees" type="button" class="gm-control-active" style="background-image: none; background-color: rgb(255, 255, 255); display: none; border: 0px; margin: 0px 0px 32px; padding: 0px; position: relative; cursor: pointer; -webkit-user-select: none; width: 40px; height: 40px; top: 0px; left: 0px; overflow: hidden; -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-top-left-radius: 2px; border-top-right-radius: 2px; border-bottom-right-radius: 2px; border-bottom-left-radius: 2px; background-position: initial initial; background-repeat: initial initial;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2224%22%20height%3D%2222%22%20viewBox%3D%220%200%2024%2022%22%3E%0A%20%20%3Cpath%20fill%3D%22%23666%22%20fill-rule%3D%22evenodd%22%20d%3D%22M20%2010c0-5.52-4.48-10-10-10s-10%204.48-10%2010v5h5v-5c0-2.76%202.24-5%205-5s5%202.24%205%205v5h-4l6.5%207%206.5-7h-4v-5z%22%20clip-rule%3D%22evenodd%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 28px; width: 28px; margin: 6px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2224%22%20height%3D%2222%22%20viewBox%3D%220%200%2024%2022%22%3E%0A%20%20%3Cpath%20fill%3D%22%23333%22%20fill-rule%3D%22evenodd%22%20d%3D%22M20%2010c0-5.52-4.48-10-10-10s-10%204.48-10%2010v5h5v-5c0-2.76%202.24-5%205-5s5%202.24%205%205v5h-4l6.5%207%206.5-7h-4v-5z%22%20clip-rule%3D%22evenodd%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 28px; width: 28px; margin: 6px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2224%22%20height%3D%2222%22%20viewBox%3D%220%200%2024%2022%22%3E%0A%20%20%3Cpath%20fill%3D%22%23111%22%20fill-rule%3D%22evenodd%22%20d%3D%22M20%2010c0-5.52-4.48-10-10-10s-10%204.48-10%2010v5h5v-5c0-2.76%202.24-5%205-5s5%202.24%205%205v5h-4l6.5%207%206.5-7h-4v-5z%22%20clip-rule%3D%22evenodd%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 28px; width: 28px; margin: 6px;"></button><button draggable="false" title="Tilt map" aria-label="Tilt map" type="button" class="gm-tilt gm-control-active" style="background-image: none; background-color: rgb(255, 255, 255); display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; -webkit-user-select: none; width: 40px; height: 40px; top: 0px; left: 0px; overflow: hidden; -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-top-left-radius: 2px; border-top-right-radius: 2px; border-bottom-right-radius: 2px; border-bottom-left-radius: 2px; background-position: initial initial; background-repeat: initial initial;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218px%22%20height%3D%2216px%22%20viewBox%3D%220%200%2018%2016%22%3E%0A%20%20%3Cpath%20fill%3D%22%23666%22%20d%3D%22M0%2C16h8V9H0V16z%20M10%2C16h8V9h-8V16z%20M0%2C7h8V0H0V7z%20M10%2C0v7h8V0H10z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="width: 18px; height: 16px; margin: 12px 11px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218px%22%20height%3D%2216px%22%20viewBox%3D%220%200%2018%2016%22%3E%0A%20%20%3Cpath%20fill%3D%22%23333%22%20d%3D%22M0%2C16h8V9H0V16z%20M10%2C16h8V9h-8V16z%20M0%2C7h8V0H0V7z%20M10%2C0v7h8V0H10z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="width: 18px; height: 16px; margin: 12px 11px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218px%22%20height%3D%2216px%22%20viewBox%3D%220%200%2018%2016%22%3E%0A%20%20%3Cpath%20fill%3D%22%23111%22%20d%3D%22M0%2C16h8V9H0V16z%20M10%2C16h8V9h-8V16z%20M0%2C7h8V0H0V7z%20M10%2C0v7h8V0H10z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="width: 18px; height: 16px; margin: 12px 11px;"></button></div></div></div></div></div><div style="background-color: white; font-weight: 500; font-family: Roboto, sans-serif; padding: 15px 25px; box-sizing: border-box; top: 5px; border: 1px solid rgba(0, 0, 0, 0.117647); border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; left: 50%; max-width: 375px; position: absolute; transform: translateX(-50%); width: calc(100% - 10px); z-index: 1;"><div><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/google_gray.svg" draggable="false" style="padding: 0px; margin: 0px; border: 0px; height: 17px; vertical-align: middle; width: 52px; -webkit-user-select: none;"></div><div style="line-height: 20px; margin: 15px 0px;"><span style="color: rgba(0, 0, 0, 0.870588); font-size: 14px;">Esta página no puede cargar Google&nbsp;Maps correctamente.</span></div><table style="width: 100%;"><tr><td style="line-height: 16px; vertical-align: middle;"><a href="https://developers.google.com/maps/documentation/javascript/error-messages?utm_source=maps_js&amp;utm_medium=degraded&amp;utm_campaign=keyless#api-key-and-billing-errors" target="_blank" rel="noopener" style="color: rgba(0, 0, 0, 0.541176); font-size: 12px;">Do you own this website?</a></td><td style="text-align: right;"><button class="dismissButton">OK</button></td></tr></table></div><div style="background-color: white; font-weight: 500; font-family: Roboto, sans-serif; padding: 15px 25px; box-sizing: border-box; top: 5px; border: 1px solid rgba(0, 0, 0, 0.117647); border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; left: 50%; max-width: 375px; position: absolute; transform: translateX(-50%); width: calc(100% - 10px); z-index: 1;"><div><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/google_gray.svg" draggable="false" style="padding: 0px; margin: 0px; border: 0px; height: 17px; vertical-align: middle; width: 52px; -webkit-user-select: none;"></div><div style="line-height: 20px; margin: 15px 0px;"><span style="color: rgba(0, 0, 0, 0.870588); font-size: 14px;">Esta página no puede cargar Google&nbsp;Maps correctamente.</span></div><table style="width: 100%;"><tr><td style="line-height: 16px; vertical-align: middle;"><a href="https://developers.google.com/maps/documentation/javascript/error-messages?utm_source=maps_js&amp;utm_medium=degraded&amp;utm_campaign=billing#api-key-and-billing-errors" target="_blank" rel="noopener" style="color: rgba(0, 0, 0, 0.541176); font-size: 12px;">Do you own this website?</a></td><td style="text-align: right;"><button class="dismissButton">OK</button></td></tr></table></div></div>
								</div>
								<!--  Comments -->
								<section class="comments">
						
									<div class="comments-content">
										<div class="content-page">
											<section class="class-details">
												<div class="comments-title">
													<div class="caveats-title">
														<h5>Instalaciones</h5>
													</div>
													<div class="caveats-full">
														<p>
															El campus Impulse Sport se desarrolla en Ripoll, en las instalaciones del CF Ripoll. Se trata de unas buenas instalaciones situadas dentro de un entorno natural de alta montaña.
														</p>
														<p>
															Impulse Sport decide repetir su campus de porteros en dicha ubicación ya que se considera un lugar idóneo para poder entrenar con la máxima calidad e intensidad en estas fechas tan veraniegas. Además del clima de alta montaña que se respira, estamos en un entorno muy familiar e íntimo, donde los porteros pueden centrarse en sus aprendizajes y convivir con sus compañeros.
														</p>
														<p>
															El campo de entrenamiento es de F-11 de hierba artificial de última generación. Disponemos de todo el campo con total exclusividad.
														</p>
														<p>
															También utilizaremos otros equipamientos deportivos como  el pavellón polideportivo, una sala de parquet para realizar estiramentos y una aula del nuevo centro cívico para realizar las charlas teóricas.
														</p>
														<p>
															Durante la semana estaremos alojados en el hotel Solana del Ter, a 15 minutos caminando del campo. El hotel tiene piscina, una sala reservada para nosotros (para jugar a la "Play Station", juegos de mesa, etc.), zona de restaurante exclusiva para el grupo y habitaciones de máximo 4 jugadores, todas con baño dentro. Toda una planta del hotel queda reservada para nuestro grupo. La web es: www.solanadelter.com
														</p>
													</div>
													<div class="caveats-content">
														<p>
															"You can design and create, and build the most wonderful place in the world. But it takes people to make the dream a reality."
														</p>
													</div>
												</div>
											</section>
										</div>

										<section class="form-comment">
											<div class="form-comment-title">
												<h5>Formulario de inscripción</h5>
											</div>
											<form id="form-comment" class="form-validate" method="post">
												<div class="form-validate-left">
													<div class="name">
														<input type="text" placeholder="Nombre Completo" name="name">
													</div>
													<div class="date">
														<input type="email" placeholder="Fecha de nacimiento" name="date">
													</div>
													<div class="age">
														<input type="text" placeholder="Edad" name="age">
													</div>
													<div class="title">
														<input type="text" placeholder="Altura" name="title">
													</div>
													<div class="title">
														<input type="text" placeholder="Peso" name="title">
													</div>
													<div class="title">
														<input type="text" placeholder="Equipo donde juega" name="title">
													</div>
													<div class="title">
														<input type="text" placeholder="Se quedará a dormir?" name="title">
													</div>
												</div>
												
												<div class="form-submit">
													<input type="submit" value="Enviar" class="btn-submit" name="submit">
												</div>												
											</form>
										</section>
									</div>
								</section>					
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
							<section class="class-trainer">
								<div class="class-trainer-title">
									<h4>Regimen Interno</h4>
									<div class="img-trainer">
										<img src="<?= base_url() ?>theme/theme/images/img-protile.png" alt=""/>
									</div>
								</div>
								<div class="class-trainer-content">
									<h4>Lupin Remus</h4>
									<h6>Fitness Trainer</h6>
									<p>Lorem ipsum dolor sit ametcont adipiscing elit. Duis vestibulum turpis eu nunc volutpat vestibulum. scelerisque arcu ac ante tristique iaculis</p>
									<a class="profile" href="#">Profile</a>
								</div>
								
							</section >
							<section class="class-trainer">
								<div class="class-trainer-title">
									<h4>Regimen Externo</h4>
									<div class="img-trainer">
										<img src="<?= base_url() ?>theme/theme/images/img-protile.png" alt=""/>
									</div>
								</div>
								<div class="class-trainer-content">
									<h4>Lupin Remus</h4>
									<h6>Fitness Trainer</h6>
									<p>Lorem ipsum dolor sit ametcont adipiscing elit. Duis vestibulum turpis eu nunc volutpat vestibulum. scelerisque arcu ac ante tristique iaculis</p>
									<a class="profile" href="#">Profile</a>
								</div>
								
							</section >
							<section class="class-trainer">
								<div class="class-trainer-title">
									<h4>Regimen Alternativo</h4>
									<div class="img-trainer">
										<img src="<?= base_url() ?>theme/theme/images/img-protile.png" alt=""/>
									</div>
								</div>
								<div class="class-trainer-content">
									<h4>Lupin Remus</h4>
									<h6>Fitness Trainer</h6>
									<p>Lorem ipsum dolor sit ametcont adipiscing elit. Duis vestibulum turpis eu nunc volutpat vestibulum. scelerisque arcu ac ante tristique iaculis</p>
									<a class="profile" href="#">Profile</a>
								</div>
								
							</section >
							<section class="class-info">
								<div class="class-info-title">
									<h4>Para todos</h4>
								</div>
								<div class="class-info-content">
									<div class="info-content">
										<span></span>
										<span>Entrenamiento mañana y tarde específico de portero.</span>
									</div>
								</div>
								<div class="class-info-content">
									<div class="info-content">
										<span></span>
										<span>Otras actividades: práctica de otros deportes (volei, fútbol-sala, baloncesto, ping-pong, etc.), piscina, cine para todos, charlas teóricas de formación y visualización de video.</span>
									</div>
								</div>
								<div class="class-info-content">
									<div class="info-content">
										<span></span>
										<span>Todos los asistentes estarán cubiertos bajo un seguro médico y de accidentes.</span>
									</div>
								</div>
								
							</section>
							
							
							<!--
							<section class="class-related">
									<div class="class-related-title">
																						<h4>Class Related</h4>
									</div>
									<div class="classes-content">
											<div class="related-product related-product-1">
													<div class="box-inner">
											<img src="<?= base_url() ?>theme/theme/images/onepage/classes-1.jpg" alt=""/>
											<div class="box-content">
												<div class="table">
													<div class="box-cell">
														<div class="title">
															<a href="our-class-details.html">
																<span>Benefits of Boxing</span>
															</a>
														</div>
														<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
														<div class="box-details">
															<a href="our-class-details.html">Details</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="name-product">
											<a class="name-product-details" href="#">Boxing</a>
										</div>
									</div>
									<div class="related-product related-product-2">
										<div class="box-inner">
											<img src="<?= base_url() ?>theme/theme/images/onepage/classes-2.jpg" alt=""/>
											<div class="box-content">
												<div class="table">
													<div class="box-cell">
														<div class="title">
															<a href="our-class-details.html">
																<span>Benefits of Boxing</span>
															</a>
														</div>
														<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
														<div class="box-details">
															<a href="our-class-details.html">Details</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="name-product">
											<a class="name-product-details" href="#">Boxing</a>
										</div>
									</div>
								</div>
								<!--div class="related-product">
								<a href="#"><img src="<?= base_url() ?>theme/theme/images/our_class_2.png"></a>
								<div class="name-product">
									<a class="name-product-details" href="#">Boxing</a>
								</div>
							</div
						</section>-->
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>