<section  class="page cms-home header-option">
	<?php $this->load->view($this->theme.'_menu_mobile',array(),FALSE,'paginas'); ?>	
	<div class="content-wrapper">
		<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>	
		<!--Banner-->
		<section class="slide-container to-top">
			<div class="ms-fullscreen-template" id="slider1-wrapper">
				<!-- masterslider -->
				<div class="master-slider ms-skin-default" id="masterslider-index">							
					<div class="ms-slide slide-1" data-delay="0">
						<div class="slide-pattern"></div>
						<video data-autopause="false" data-mute="true" data-loop="true" data-fill-mode="fill">
							<source src="<?= base_url() ?>theme/theme/images/video/demo.mp4" type="video/mp4"/>									
						</video>  
						<h3 class="ms-layer hps-title1" style="left:101px"
							data-type="text"
							data-ease="easeOutExpo"
							data-delay="1000"
							data-duration="0"
							data-effect="skewleft(30,80)"
						>
							Entrenamiento y representación de porteros de fútbol
						</h3>																												
						<h3 class="ms-layer hps-title3" style="left:95px"
							data-type="text"
							data-delay="1900"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,40,t)"
							data-ease="easeOutExpo"
						>
							Descubre, aprende...
						</h3>
						
						<h3 class="ms-layer hps-title4" style="left:101px"
							data-type="text"
							data-delay="2500"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,18,t)"
							data-ease="easeOutExpo"
						>
							Más información
						</h3>
					</div>
					<div class="ms-slide slide-2" data-delay="0">
					   <div class="slide-pattern"></div>							  
						<img src="<?= base_url() ?>theme/theme/images/blank.gif" data-src="<?= base_url() ?>theme/theme/images/bg_1.jpg" alt="lorem ipsum dolor sit"/>
						<h3 class="ms-layer hps-title1" style="left:101px"
							data-type="text"
							data-ease="easeOutExpo"
							data-delay="1000"
							data-duration="0"
							data-effect="skewleft(30,80)">
							Athlete Fitness Club
						</h3>																												
						<h3 class="ms-layer hps-title3" style="left:95px"
							data-type="text"
							data-delay="1900"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,40,t)"
							data-ease="easeOutExpo"
						>
							Make You Be The Fighter
						</h3>
						
						<h3 class="ms-layer hps-title4" style="left:101px"
							data-type="text"
							data-delay="2500"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,18,t)"
							data-ease="easeOutExpo"
						>
							Try A Free Class
						</h3>
					</div>	
					
					<div class="ms-slide slide-3" data-delay="0">
					   <div class="slide-pattern"></div>							  
						<img src="<?= base_url() ?>theme/theme/images/blank.gif" data-src="<?= base_url() ?>theme/theme/images/bg-home-v2.jpg" alt="lorem ipsum dolor sit"/>
						<h3 class="ms-layer hps-title1" style="left:101px"
							data-type="text"
							data-ease="easeOutExpo"
							data-delay="1000"
							data-duration="0"
							data-effect="skewleft(30,80)"
						>
							Athlete Fitness Club
						</h3>																												
						<h3 class="ms-layer hps-title3" style="left:95px"
							data-type="text"
							data-delay="1900"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,40,t)"
							data-ease="easeOutExpo"
						>
							Make You Be The Fighter
						</h3>
						
						<h3 class="ms-layer hps-title4" style="left:101px"
							data-type="text"
							data-delay="2500"
							data-duration="0"
							data-effect="rotate3dtop(-100,0,0,18,t)"
							data-ease="easeOutExpo"
						>
							Try A Free Class
						</h3>
					</div>							
				</div>
				<!-- end of masterslider -->
				<div class="to-bottom" id="to-bottom"><i class="fa fa-angle-down"></i></div>
			</div>
		</section>

















		<!--End Banner-->										
		<div class="contents-main" id="contents-main">				
			<!--Collection-->
				<div class="collection">
					<div class="container">
						<div class="row">									
							<div class="collection-content">
								<div class="col-md-4 col-sm-12 col-xs-12 box">											
									<div class="women-content">
										<div class="masonry-small">
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/newproducts/images-3.png">
											</div>
											<div class="price-table-text">
												<h3>Campus</h3>
												<h2>internacionales</h2>
												<div class="border-bottom"></div>
												<p>Implementamos nuestro programa de entrenamiento en diferentes países.</p>
												<span><span>info</span></span>
											</div>
										</div>
										<div class="masonry-lagar">
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/store/img-shop.jpg">
											</div>
											<div class="price-table-text">
												<div class="masonry-lagar-content">
													<div class="title-masory">
														<h3>2019</h3>
														<h2>Escuela de Porteros</h2>
														<div class="border-bottom"></div>
													</div>
													<div class="text-masony">
														<p>Podrás ir descubriendo, aprendiendo y dominando todos los recursos técnicos y tácticos del portero de fútbol.</p>
														<span><span>info</span></span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 col-sm-12 col-xs-12 box">											
									<div class="man-content">
										<div class="masonry-lagar">
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/store/img-shop-2.jpg">
											</div>
											<div class="price-table-text">
												<div class="masonry-lagar-content">
													<div class="title-masory">
														<!-- 
<h3>2015</h3>
														<h2>Best Selling</h2>
 -->
														<!-- <div class="border-bottom"></div> -->
													</div>
													<div class="text-masony">
														<p>Durante las navidades organizamos campus exclusivo para perfeccionar vuestras habilidades en la portería.</p>
														<span><span>info</span></span>
													</div>
												</div>
											</div>
										</div>
										<div class="masonry-small">
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/newproducts/images-2.png">
											</div>
											<div class="price-table-text">
												<h3>Entrenamiento</h3>
												<h2>personal</h2>
												<div class="border-bottom"></div>
												<p>Dirigido a aquellos porteros que quieren optimizar al máximo su perfil de juego para obtener el máximo rendimiento deportivo.</p>
												<span><span>info</span></span>
											</div>
										</div>												
									</div>
								</div>
								<div class="col-md-4 col-sm-12 col-xs-12 box">											
									<div class="collect-content">
										<div class="masonry-small">
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/newproducts/images-1.png">
											</div>
											<div class="price-table-text">
												<h3>Asesoramiento</h3>
												<h2>institucional</h2>
												<div class="border-bottom"></div>
												<p>Ayudamos a clubes y federaciones a organizar su área de porteros y a formar a sus entrenadores y responsables.</p>
												<span><span>info</span></span>
											</div>
										</div>	
										<div class="masonry-lagar">													
											<div class="price-table-img">
												<img alt="" src="<?= base_url() ?>theme/theme/images/store/img-shop-3.jpg">
											</div>
											<div class="price-table-text">
												<div class="masonry-lagar-content">
													<div class="title-masory">
														<h3>curso de</h3>
														<h2>entrenadores</h2>
														<div class="border-bottom"></div>
													</div>
													<div class="text-masony">
														<p>Ofrecemos cursos a clubes, federaciones o individuos de forma particular para formar y mejorar el conocimiento sobre la demarcación del portero de fútbol, su planificación y su entrenamiento.</p>
														<!-- <span>From <span> $59</span></span> -->
														<span><span>info</span></span>
													</div>
												</div>
											</div>
										</div>																							
									</div>
								</div>
							</div>
							<!--- --->
						</div>
					</div>
				</div>
			<!--End Collection-->
















			<!--Training-->
			<section class="training scroll-to">
				<!-- PARALLAX WINDOWS -->
				<div class="parallax-block-wrap-module auto-width">
					<div class="parallax-block bt-advance-parallax" id="parallax-block">
						
						<!-- PARALLAX CONTENT -->
						<div class="control-button">
							<div class="nav-wrap hidden">
								<div class="nav-wrap-in next">
									<span class="nav-next"></span>
								</div>
								<div class="nav-wrap-in prev">
									<span class="nav-prev"></span>
								</div>
							</div>
							<div style="display: none;" class="button-wrap">
								<span class="button close-btn"></span>
							</div>
						</div>
						<div class="parallax-background">
							<!--Textured and color-->
							<div class="parallax-background-overlay"></div>
							<img alt="" src="<?= base_url() ?>theme/theme/images/bg-position-3.1.jpg">										
						</div>
						
						<div class="parallax-block-content default-pos parallax-text parallax-v2">									
							<div class="container">
								<p class="strart-your">Inscríbete</p>
								<h1 class="training-title">ahora mismo</h1>
								<span class="button open-btn"><span>Formulario</span></span>
							</div>
						</div>
						
						<div class="overlay-loading"><span class="loading"><img src="<?= base_url() ?>theme/theme/images/loading-black.gif" alt=""/></span></div>    

						<!-- POPUP CONTENT -->
						<div  class="parallax-content-wrap">
							<div class="parallax-content hidden">
								<div class="parallax-content-in">
								</div>
								<div class="content-show-large">
									<div class="item-contain">
										<div class="video-inner">
											<!-- VIDEO TAG OR EMBED CODE -->
											<video data-fill-mode="fill" data-loop="true" data-autopause="false" loop>
												<source type="video/mp4" src="<?= base_url() ?>theme/theme/images/video/demo.mp4" />									
											</video> 	
										</div>
									</div>
									<div class="loading"><img src="<?= base_url() ?>theme/theme/images/loading-black.gif" alt=""/></div>
								</div>
							</div>
						</div>
						<!-- end POPUP CONTENT -->
											  
					</div>
				</div>	
			</section>
			<!--End Training-->
			









			<!--Services-->		
			<section class="services">
				<div class="container">
					<div class="row">
						<div class="title-page">
							<h4><span>SERVICIOS</span></h4>
						</div>
						<div class="sevices-main">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="sevices-wapper">	
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-1.png" alt=""/>
										</div>
										<h4 class="services-title">ESCUELA</h4>
										<hr class="border-title">
										<h3 class="services-title">de porteros</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Entrenamiento semanal para mejorar y reforzar todos aquellos conceptos de juego y habilidades que necesitas desarrollar en el juego.</p>
										</div>										
									</div>										
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="sevices-wapper">	
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-2.png" alt=""/>
										</div>
										<h4 class="services-title">campus</h4>
										<hr class="border-title">
										<h3 class="services-title">de porteros</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>En nuestro campus vas a entrenar de forma específica las diferentes habilidades técnicas, tácticas y coordinativas esenciales para nuestra demarcación.</p>
										</div>	
									</div>										
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="sevices-wapper">	
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-3.png" alt=""/>
										</div>
										<h4 class="services-title">entrenamiento</h4>
										<hr class="border-title">
										<h3 class="services-title">personal</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Dirigido a aquellos porteros que quieren optimizar al máximo su perfil de juego para obtener el máximo rendimiento deportivo.</p>
										</div>	
									</div>										
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="sevices-wapper">	
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-4.png" alt=""/>
										</div>
										<h4 class="services-title">representación</h4>
										<hr class="border-title">
										<h3 class="services-title">deportiva</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Entendemos la representación deportiva como un servicio integral, específico y único para esta demarcación y ofrecemos tales servicios para garantizar lo mejor.</p>
										</div>	
									</div>								
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="sevices-wapper">		
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-5.png" alt=""/>
										</div>
										<h4 class="services-title">curso</h4>
										<hr class="border-title">
										<h3 class="services-title">de entrenadores</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Ofrecemos cursos a clubes, federaciones o individuos de forma particular para formar y mejorar el conocimiento sobre la demarcación del portero de fútbol...</p>
										</div>	
									</div>										
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="sevices-wapper">	
									<div class="services-content">
										<div class="intro-img">
											<img src="<?= base_url() ?>theme/theme/images/icon-6.png" alt=""/>
										</div>
										<h4 class="services-title">asesoramiento</h4>
										<hr class="border-title">
										<h3 class="services-title">institucional</h3>
										<hr  class="border-title-1">
										<div class="actions">
											<p>Ayudamos a clubes y federaciones a organizar su área de porteros y a formar a sus entrenadores y responsables.</p>
										</div>	
									</div>										
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
			<!--End Services-->	







			<!-- Athlete Facts -->
					<section class="facts image-bg-2 ">
						<div class="background-overlay"></div>
						<div class="container">
							<div class="row">
								<div class="facts-page">
									<div class="title-page title-facts">
										<h3 class="module-title-h3">Entra en Impulse Sport</h3>
										<p>Son muchos los deportistas que han apostado por nuestra escuela y han obtenido resultados. Aprende, perfecciona, conoce y descubre en Impulse Sport. </p>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="facts-content">
											<div class="facts-icon"><i class="fa fa-bolt"></i></div>
											<div class="count">
												<span id="counter-1">600</span>
												<div class="facts-border"></div>
											</div>
											<p class="facts-text">Porteros</p>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="facts-content">
											<div class="facts-icon"><i class="fa fa-anchor"></i></div>
											<div class="count">
												<span id="counter-2">30</span>
												<div class="facts-border"></div>
											</div>
											<p class="facts-text">Clubs</p>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="facts-content">
											<div class="facts-icon"><i class="fa fa-drupal"></i></div>
											<div class="count">
												<span id="counter-3">5000</span>
												<div class="facts-border"></div>
											</div>
											<p class="facts-text">Horas de entreno</p>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="facts-content">
											<div class="facts-icon"><i class="fa fa-bomb "></i></div>
											<div class="count">
												<span id="counter-4">10</span>
												<div class="facts-border"></div>
											</div>
											<p class="facts-text">Campus</p>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</section>
					<!-- End Athlete Facts -->			
					
				<div id="class">
					<!-- Athlete Class -->
					<section id="classes" class="classes">
						<div class="classes-content">
							<div class="classes-wapper">
								<div class="title-page">
									<h3 class="module-title-h3">Visita nuestra galería</h3>
									<p>Conoce nuestras actividades, nuestros campus, nuestra escuela a través de las imágenes </p>
								</div>
							</div>
							<div id="filters" class="filters button-group container">							
								<button  data-filter="*" class="filter button is-checked">Todas</button>
								<button  data-filter=".aerobic" class="filter button">Campus Verano</button>
								<button  data-filter=".fitness" class="filter button">Campus Navidad</button>
								<button  data-filter=".sport" class="filter button">Entrenamientos</button>
								<button  data-filter=".boxing" class="filter button">Internacional</button>									
								<button  data-filter=".dance" class="filter button">Escuela</button>																		
							</div>
							<div class="class-our">				
							<div class="classes-athlete">
								<div class="classes-inner">
									<div id="filtering-demo">			
										<section id="our-class-main" class="isotope">
											<div class="mix dance element-item col-xs-12 col-sm-6 col-md-4" data-category="dance">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-6.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="our-class-details.html">
																		<span>Grupo Campus</span>
																	</a>
																</div>
																<div class="box-text">Participantes Campus Verano 2013</div>
																<div class="box-details">
																	<a href="our-class-details.html">Detalles</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>
											<div class="mix sport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-1.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="our-class-details.html">
																		<span>Trabajo parada baja</span>
																	</a>
																</div>
																<div class="box-text">Trabajo específico de parada en balón bajo</div>
																<div class="box-details">
																	<a href="our-class-details.html">Detalles</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>												
											<div class="mix fitness sport element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-3.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="our-class-details.html">
																		<span>Campus Verano 2018</span>
																	</a>
																</div>
																<div class="box-text">Ejercicio de chute-parada</div>
																<div class="box-details">
																	<a href="our-class-details.html">Detalles</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>											
											<div class="mix fsport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="boxing">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-4.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="#">
																		<span>Campus Japón 2016</span>
																	</a>
																</div>
																<div class="box-text">Campus Internacional en Japón</div>
																<div class="box-details">
																	<a href="our-class-details.html">Detalles</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>
											<div class="mix aerobic fitness element-item col-xs-12 col-sm-6 col-md-4" data-category="aerobic">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-5.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="">
																		<span>Escuela de porteros</span>
																	</a>
																</div>
																<div class="box-text">Entrenamiento temporada 2017-2018</div>
																<div class="box-details">
																	<a href="our-class-details.html">Detalles</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>												
											<div class="mix fitness belly element-item col-xs-12 col-sm-6 col-md-4" data-category="belly">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-2.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="our-class-details.html">
																		<span>Campus Navidad 2015</span>
																	</a>
																</div>
																<div class="box-text">Navidad en Can Vidalet</div>
																<div class="box-details">
																	<a href="our-class-details.html">Detalles</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>
											<div class="mix fitness sport element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-10.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="#">
																		<span>Escuela de porteros</span>
																	</a>
																</div>
																<div class="box-text">Entrenamiento temporada 2017-2018</div>
																<div class="box-details">
																	<a href="#">Detalles</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>													
											<div class="mix sport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="boxing">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-8.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="#">
																		<span>Grupo Campus Japón 2016</span>
																	</a>
																</div>
																<div class="box-text">Grupo participante del campus en Japón</div>
																<div class="box-details">
																	<a href="#">Detalles</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>
											<div class="mix aerobic fitness element-item col-xs-12 col-sm-6 col-md-4" data-category="aerobic">
												<div class="box-inner">
													<img src="<?= base_url() ?>theme/theme/images/onepage/classes-9.jpg" alt=""/>
													<div class="box-content">
														<div class="table">
															<div class="box-cell">
																<div class="title">
																	<a href="#">
																		<span>Pretemporada</span>
																	</a>
																</div>
																<div class="box-text">Pretemporada con porteros amateurs</div>
																<div class="box-details">
																	<a href="#">Detalles</a>
																</div>
															</div>
														</div>	
													</div>
												</div>										
											</div>									
										</section>										
									</div>
								</div>
							</div>
						</div>
					</div>
					</section>
					<!-- End Athlete Class -->
				</div>
			<!--News-->	
			<div class="home-v2">
				<section class="news-page"  style="padding-bottom: 0">
					<div class="container">
						<div class="row">							
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="news-cont">
									<div class="title-page">
										<h4>Últimas noticias</h4>
										<h3>Nuestro Blog</h3>									
										<									
									</div>
									<!-- 
<p class="news-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla.
									<a href="#">Click here to see all</a></p>
 -->
									<div class="news-wapper">
										<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="img-news">
												<img src="<?= base_url() ?>theme/theme/images/img-1.png" alt=""/>
											</div>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<div class="details-news">
												<h6>Boxing</h6>
												<a href="#">Los Angeles Event</a>
												<p>Consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla.</p>
											</div>
										</div>
									</div>
									<div class="news-wapper">
										<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="img-news">
												<img src="<?= base_url() ?>theme/theme/images/img-2.png" alt=""/>
											</div>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<div class="details-news">
												<h6>Boxing</h6>
												<a href="#">Los Angeles Event</a>
												<p>Consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla.</p>
											</div>
										</div>
									</div>
									<div class="news-wapper">
										<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="img-news">
												<img src="<?= base_url() ?>theme/theme/images/img-2.png" alt=""/>
											</div>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<div class="details-news">
												<h6>Boxing</h6>
												<a href="#">Los Angeles Event</a>
												<p>Consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla.</p>
											</div>
										</div>
									</div>

									<div class="news-wapper">
										<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="img-news">
												<img src="<?= base_url() ?>theme/theme/images/img-2.png" alt=""/>
											</div>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<div class="details-news">
												<h6>Boxing</h6>
												<a href="#">Los Angeles Event</a>
												<p>Consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla.</p>
											</div>
										</div>
									</div>

									<div class="news-wapper">
										<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="img-news">
												<img src="<?= base_url() ?>theme/theme/images/img-2.png" alt=""/>
											</div>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-12">
											<div class="details-news">
												<h6>Boxing</h6>
												<a href="#">Los Angeles Event</a>
												<p>Consectetur adipiscing elit. Nunc sit amet pulvinar nulla, quis facilisis nulla.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="title-page col-md-6 col-sm-6 col-xs-12">
								<div class="news-cont">
								<div class="about-btm-left about-box social-feed-container">
										<div class="about-bottom-title">
											<h4>Facebook</h4>
										</div>
										
									</div>
									
								</div>
							</div>
						</div>
					</div>

					<section class="our-partners" style="padding:20px 0; margin-bottom: 0; background: #d9232e; margin-top: 40px;">
				<div class="container">
					<div class="row">
						<div class="headding-title">
							<h4>Patrocinadores</h4>
							<div class="headding-bottom"></div>
						</div>
						<div class="brand">
							<div class="col-md-2 col-sm-4 col-xs-12">
								<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-02.png" alt=""></div>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-12">
								<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-03.png" alt=""></div>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-12">
								<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-04.png" alt=""></div>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-12">
								<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-05.png" alt=""></div>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-12">
								<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-06.png" alt=""></div>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-12">
								<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-02.png" alt=""></div>
							</div>
						</div>
					</div>
				</div>
			</section>	
				</section>


			</div>
			
							
		</div>
	</div>
</section>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/masterslider.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/banner.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/theme.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/custom.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/waypoints.js"></script>	
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/template.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/main.js"></script>



<script src="<?= base_url() ?>js/social/bower_components/codebird-js/codebird.js"></script>
<!-- doT.js for rendering templates -->
<script src="<?= base_url() ?>js/social/bower_components/doT/doT.min.js"></script>
<!-- Moment.js for showing "time ago" and/or "date"-->
<script src="<?= base_url() ?>js/social/bower_components/moment/min/moment.min.js"></script>
<!-- Moment Locale to format the date to your language (eg. italian lang)-->
<script src="<?= base_url() ?>js/social/bower_components/moment/locale/it.js"></script>
<!-- Social-feed js -->
<script src="<?= base_url() ?>js/social/js/jquery.socialfeed.js"></script>
<script>
    $(document).ready(function(){
        $('.social-feed-container').socialfeed({            
            // INSTAGRAM
            facebook:{
                accounts: ['@mallorcaislandfestival'],  //Array: Specify a list of accounts from which to pull wall posts
                limit: 5,                                   //Integer: max number of posts to load
                access_token: 'EAAOL3DoVcQABAIh31PNOSsakf3b1lYKYP10TsB8RZBs6wsSdciiEwgdzxVk2tqqJ3bFiyZBa9JJBSzK0SwF7zmBBZC7FTrSHvWsgmPD0gK0RQNfqQd6eMJbYBHbcwyE4UiWtBhYnu2Jjrif2eeU1E6VqERYEb1VdROjzCKuRgZDZD'  //String: "APP_ID|APP_SECRET"
            },
            
            // GENERAL SETTINGS
            length:400,                                     //Integer: For posts with text longer than this length, show an ellipsis.
            show_media:true,                                //Boolean: if false, doesn't display any post images
            media_min_width: 300,                           //Integer: Only get posts with images larger than this value
            update_period: 5000,                            //Integer: Number of seconds before social-feed will attempt to load new posts.
            template: "<?= base_url() ?>js/social/js/template.html",                         //String: Filename used to get the post template.            
            date_format: "ll",                              //String: Display format of the date attribute (see http://momentjs.com/docs/#/displaying/format/)
            date_locale: "en",                              //String: The locale of the date (see: http://momentjs.com/docs/#/i18n/changing-locale/)
            moderation: function(content) {                 //Function: if returns false, template will have class hidden
                return  (content.text) ? content.text.indexOf('fuck') == -1 : true;
            },
            callback: function() {                          //Function: This is a callback function which is evoked when all the posts are collected and displayed
                /*$(".isotope-grid").isotope('reloadItems');
                $(".isotope-grid").isotope('layout');
                setTimeout(function(){
                    $("#allSocial").trigger('click');
                    $("a[data-rel^=lightcase]").lightcase({showSequenceInfo:!1,swipe:!0,showCaption:!1,overlayOpacity:.95,maxWidth:1300,maxHeight:1100,shrinkFactor:1,video:{width:780,height:420}});
                },2000);*/
            }
        });
    });
</script>
