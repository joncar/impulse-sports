<section  class="page page-category header-option">
	<?php $this->load->view($this->theme.'_menu_mobile',array(),FALSE,'paginas'); ?>	
	<div class="content-wrapper">
		<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>	
		
						<!--Banner-->
				<section class="page-heading">
					<div class="title-slide">
						<div class="container">
								<div class="banner-content slide-container">									
									<div class="page-title">
										<h3>Our Blog</h3>
									</div>
								</div>
						</div>
					</div>
				</section>
				<!--End Banner-->
				<div class="page-content">					
					<!-- Breadcrumbs -->
					<div class="breadcrumbs">
						<div class="container">
							<div class="row">
								<div class="col-md-9">
									<ul>
										<li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
										<li><span>//</span></li>
										<li class="category-1"><a href="#">Page!</a></li>
										<li><span>//</span></li>
										<li class="category-2">Our class</li>								
									</ul>
								</div>
							</div>							
						</div>
					</div>
					<!-- End Breadcrumbs -->
					
					<!-- Athlete Class -->
					<section id="classes" class="classes">
						<div class="categories">
							<div id="filters" class="filters button-group container">							
								<button  data-filter="*" class="filter button is-checked">All Class</button>
								<button  data-filter=".aerobic" class="filter button">Yoga Class</button>
								<button  data-filter=".fitness" class="filter button">Fitness Class</button>
								<button  data-filter=".sport" class="filter button">Sport Class</button>
								<button  data-filter=".boxing" class="filter button">Boxing Class</button>									
							</div>
						</div>
						<div class="our-class-main">
							<div class="container">				
								<div class="row">				
									<div class="classes-athlete">
										<div class="classes-inner">
											<div id="filtering-demo" class="classes-content">			
												<section id="our-class-main" class="isotope">
													<div class="mix sport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
														<div class="box-inner">
															<img src="<?= base_url() ?>theme/theme/images/onepage/classes-1.jpg" alt=""/>
															<div class="box-content">
																<div class="table">
																	<div class="box-cell">
																		<div class="title">
																			<a href="class-details.html">
																				<span>Benefits of Boxing</span>
																			</a>
																		</div>
																		<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																		<div class="box-details">
																			<a href="class-details.html">Details</a>
																		</div>
																	</div>
																</div>	
															</div>
														</div>										
													</div>												
													<div class="mix fitness sport element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
														<div class="box-inner">
															<img src="<?= base_url() ?>theme/theme/images/onepage/classes-2.jpg" alt=""/>
															<div class="box-content">
																<div class="table">
																	<div class="box-cell">
																		<div class="title">
																			<a href="class-details.html">
																				<span>Benefits of Boxing</span>
																			</a>
																		</div>
																		<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																		<div class="box-details">
																			<a href="class-details.html">Details</a>
																		</div>
																	</div>
																</div>	
															</div>
														</div>										
													</div>
													<div class="mix aerobic fitness element-item col-xs-12 col-sm-6 col-md-4" data-category="fitness">
														<div class="box-inner">
															<img src="<?= base_url() ?>theme/theme/images/onepage/classes-3.jpg" alt=""/>
															<div class="box-content">
																<div class="table">
																	<div class="box-cell">
																		<div class="title">
																			<a href="class-details.html">
																				<span>Benefits of Boxing</span>
																			</a>
																		</div>
																		<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																		<div class="box-details">
																			<a href="class-details.html">Details</a>
																		</div>
																	</div>
																</div>	
															</div>
														</div>										
													</div>
													<div class="mix fsport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="boxing">
														<div class="box-inner">
															<img src="<?= base_url() ?>theme/theme/images/onepage/classes-4.jpg" alt=""/>
															<div class="box-content">
																<div class="table">
																	<div class="box-cell">
																		<div class="title">
																			<a href="#">
																				<span>Benefits of Boxing</span>
																			</a>
																		</div>
																		<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																		<div class="box-details">
																			<a href="class-details.html">Details</a>
																		</div>
																	</div>
																</div>	
															</div>
														</div>										
													</div>
													<div class="mix aerobic fitness element-item col-xs-12 col-sm-6 col-md-4" data-category="aerobic">
														<div class="box-inner">
															<img src="<?= base_url() ?>theme/theme/images/onepage/classes-5.jpg" alt=""/>
															<div class="box-content">
																<div class="table">
																	<div class="box-cell">
																		<div class="title">
																			<a href="">
																				<span>Benefits of Boxing</span>
																			</a>
																		</div>
																		<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																		<div class="box-details">
																			<a href="class-details.html">Details</a>
																		</div>
																	</div>
																</div>	
															</div>
														</div>										
													</div>
													<div class="mix aerobic fitness element-item col-xs-12 col-sm-6 col-md-4" data-category="fitness">
														<div class="box-inner">
															<img src="<?= base_url() ?>theme/theme/images/onepage/classes-6.jpg" alt=""/>
															<div class="box-content">
																<div class="table">
																	<div class="box-cell">
																		<div class="title">
																			<a href="#">
																				<span>Benefits of Boxing</span>
																			</a>
																		</div>
																		<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																		<div class="box-details">
																			<a href="#">Details</a>
																		</div>
																	</div>
																</div>	
															</div>
														</div>										
													</div>
													<div class="mix fitness sport element-item col-xs-12 col-sm-6 col-md-4" data-category="sport">
														<div class="box-inner">
															<img src="<?= base_url() ?>theme/theme/images/onepage/classes-10.jpg" alt=""/>
															<div class="box-content">
																<div class="table">
																	<div class="box-cell">
																		<div class="title">
																			<a href="#">
																				<span>Benefits of Boxing</span>
																			</a>
																		</div>
																		<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																		<div class="box-details">
																			<a href="#">Details</a>
																		</div>
																	</div>
																</div>	
															</div>
														</div>										
													</div>													
													<div class="mix sport boxing element-item col-xs-12 col-sm-6 col-md-4" data-category="boxing">
														<div class="box-inner">
															<img src="<?= base_url() ?>theme/theme/images/onepage/classes-8.jpg" alt=""/>
															<div class="box-content">
																<div class="table">
																	<div class="box-cell">
																		<div class="title">
																			<a href="#">
																				<span>Benefits of Boxing</span>
																			</a>
																		</div>
																		<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																		<div class="box-details">
																			<a href="#">Details</a>
																		</div>
																	</div>
																</div>	
															</div>
														</div>										
													</div>
													<div class="mix aerobic fitness element-item col-xs-12 col-sm-6 col-md-4" data-category="aerobic">
														<div class="box-inner">
															<img src="<?= base_url() ?>theme/theme/images/onepage/classes-9.jpg" alt=""/>
															<div class="box-content">
																<div class="table">
																	<div class="box-cell">
																		<div class="title">
																			<a href="#">
																				<span>Benefits of Boxing</span>
																			</a>
																		</div>
																		<div class="box-text">Lorem ipsum dolor sit amet, consectetur adipiscing...</div>
																		<div class="box-details">
																			<a href="#">Details</a>
																		</div>
																	</div>
																</div>	
															</div>
														</div>										
													</div>									
												</section>										
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="load-product">
							<div class="container">
								<div class="row">
									<div class="col-md-12">
										<button id="load-more-class" class="load-more">Load More</button>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- End Athlete Class -->
				</div>
		
	</div>
</section>





<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>