<section  class="page page-category header-option">
	<?php $this->load->view($this->theme.'_menu_mobile',array(),FALSE,'paginas'); ?>	
	<div class="content-wrapper">
		<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>	
		
		<!--Banner-->
				<section class="page-heading">
					<div class="title-slide">
						<div class="container">
								<div class="banner-content slide-container">									
									<div class="page-title">
										<h3>Our Trainers</h3>
									</div>
								</div>
						</div>
					</div>
				</section>
				<!--End Banner-->
				<div class="page-content">					
					<!-- Breadcrumbs -->
					<div class="breadcrumbs">
						<div class="container">
							<div class="row">
								<div class="col-md-9">
									<ul>
										<li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
										<li><span>//</span></li>
										<li class="category-1"><a href="#">Page!</a></li>
										<li><span>//</span></li>
										<li class="category-2">Our trainner</li>								
									</ul>
								</div>
							</div>	
						</div>
					</div>
					<!-- End Breadcrumbs -->
										<!-- Athlete Class -->
					<section id="classes" class="classes">						
						<div class="container">						
							<div class="classes-athlete">
								<div class="classes-inner choose">
									<div class="classes-content">									
										<div class="container">
											<div class="row">
												<div class="col-md-6 col-sm-12 col-xs-12">
													<div class="block-left">
														<div class="about-img">
															<img src="<?= base_url() ?>theme/theme/images/bg-about-us.png" alt=""/>
														</div>
														<div class="about-title">
															<h3>Why choose athlete club</h3>
														</div>
														<div class="about-desc">
															<p>
															Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean egestas eu leo nec 
															eleifend. Nulla sapien turpis, convallis ac ultrices sed, aliquam sit amet arcu. Phasellus est 
															orci, bibendum consequat quam sit amet, facilisis pharetra nisl. Nullam ultrices mi quam, et 
															volutpat magna suscipit eget.
															</p>
														</div>
													</div>
												</div>
												<div class="col-md-6 col-sm-12 col-xs-12">
													<div class="block-right">
														<div class="block-right-info">
															<div class="about-info">
																<div class="icon-block">
																	<i class="fa fa-tablet"></i>
																</div>
																<div class="about-details">
																	<h4 class="about-details-title">Full responsive and retina ready</h4>
																	<p>
																	Consectetur adipiscing elit. Pellentesque ut mollis lectus. Ut ultricies lacinia feugiat. 
																	Cras sed enim quis nunc condimentum ullamcorper.
																	Aliquam eget lorem at purus. Etiam tempus porta mauris at fermentum.
																	</p>
																</div>
															</div>
															<div class="about-info">
																<div class="icon-block">
																	<i class="fa fa-paint-brush"></i>
																</div>
																<div class="about-details">
																	<h4 class="about-details-title">Unique and creative design</h4>
																	<p>
																	Consectetur adipiscing elit. Pellentesque ut mollis lectus. Ut ultricies lacinia feugiat. 
																	Cras sed enim quis nunc condimentum ullamcorper.
																	Aliquam eget lorem at purus. Etiam tempus porta mauris at fermentum.
																	</p>
																</div>
															</div>
															
														</div>
													</div>
												</div>																																																		
											</div>
										</div>																				
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- End Athlete Class -->
					<!-- Main Content -->
					<div class="main-content">
						<div class="container">
							<div class="row">
								<section id="our-trainers" class="our-trainers">
									<div class="our-trainer-box col-md-4 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image product-trainer">
													<a href="<?= base_url() ?>servicios_detalles.html"><img src="<?= base_url() ?>theme/theme/images/our_trainers_1.png" alt=""/></a>
												</div>
												<div class="info-products">
													<div class="img-trainers">
														<img src="<?= base_url() ?>theme/theme/images/boxing-icon-1.png" alt=""/>
													</div>
													<div class="product-name" >
														<a href="<?= base_url() ?>servicios_detalles.html">Jonh Mike</a>
														<div class="product-bottom"></div>
													</div>
													<div class="product-info">																										
														<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.  </p>
													</div>												
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-facebook"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube"></i></a></li>
														</ul>
													</div>
												</div>												
											</div>								
										</div>						
									</div>
									<div class="our-trainer-box col-md-4 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image product-trainer">
													<a href="<?= base_url() ?>servicios_detalles.html"><img src="<?= base_url() ?>theme/theme/images/our_class_4.png" alt=""/></a>
												</div>
												<div class="info-products">
													<div class="img-trainers">
														<img src="<?= base_url() ?>theme/theme/images/boxing-icon-2.png" alt=""/>
													</div>
													<div class="product-name" >
														<a href="<?= base_url() ?>servicios_detalles.html">Jonh Mike</a>
														<div class="product-bottom"></div>
													</div>
													<div class="product-info">																										
														<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.  </p>
													</div>												
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-facebook"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube"></i></a></li>
														</ul>
													</div>
												</div>												
											</div>																																											
										</div>																																											
									</div>
									<div class="our-trainer-box col-md-4 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image product-trainer">
													<a href="<?= base_url() ?>servicios_detalles.html"><img src="<?= base_url() ?>theme/theme/images/our_class_2.png" alt=""/></a>
												</div>
												<div class="info-products">
													<div class="img-trainers">
														<img src="<?= base_url() ?>theme/theme/images/boxing-icon-3.png" alt=""/>
													</div>
													<div class="product-name" >
														<a href="<?= base_url() ?>servicios_detalles.html">Jonh Mike</a>
														<div class="product-bottom"></div>
													</div>
													<div class="product-info">																										
														<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.  </p>
													</div>												
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-facebook"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube"></i></a></li>
														</ul>
													</div>
												</div>												
											</div>																																											
										</div>																																											
									</div>
									<div class="our-trainer-box col-md-4 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image product-trainer">
													<a href="#"><img alt="" src="<?= base_url() ?>theme/theme/images/our_trainers_4.png"></a>
												</div>
												<div class="info-products">
													<div class="img-trainers">
														<img src="<?= base_url() ?>theme/theme/images/boxing-icon-4.png" alt=""/>
													</div>
													<div class="product-name" >
														<a href="">Jonh Mike</a>
														<div class="product-bottom"></div>
													</div>
													<div class="product-info">																										
														<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.  </p>
													</div>												
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-facebook"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube"></i></a></li>
														</ul>
													</div>
												</div>												
											</div>																																											
										</div>																																											
									</div>
									<div class="our-trainer-box col-md-4 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image product-trainer">
													<a href="#"><img alt="" src="<?= base_url() ?>theme/theme/images/our_trainers_2.png"></a>
												</div>
												<div class="info-products">
													<div class="img-trainers">
														<img src="<?= base_url() ?>theme/theme/images/boxing-icon-5.png" alt=""/>
													</div>
													<div class="product-name" >
														<a href="<?= base_url() ?>servicios_detalles.html">Jonh Mike</a>
														<div class="product-bottom"></div>
													</div>
													<div class="product-info">																										
														<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.  </p>
													</div>												
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-facebook"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube"></i></a></li>
														</ul>
													</div>
												</div>												
											</div>																																											
										</div>																																											
									</div>
									<div class="our-trainer-box col-md-4 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image product-trainer">
													<a href="<?= base_url() ?>servicios_detalles.html"><img src="<?= base_url() ?>theme/theme/images/our_trainers_1.png" alt=""/></a>
												</div>
												<div class="info-products">
													<div class="img-trainers">
														<img src="<?= base_url() ?>theme/theme/images/boxing-icon-6.png" alt=""/>
													</div>
													<div class="product-name" >
														<a href="<?= base_url() ?>servicios_detalles.html">Jonh Mike</a>
														<div class="product-bottom"></div>
													</div>
													<div class="product-info">																										
														<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.  </p>
													</div>												
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-facebook"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube"></i></a></li>
														</ul>
													</div>
												</div>												
											</div>																																											
										</div>																																											
									</div>
									
								</section>								
							</div>
						</div>
					</div>
					<!-- Main Content -->
				</div>
		
	</div>
</section>





<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>