<!--Header-->
<header id="header" class="header header-container header-container-3  alt reveal">
	<!--Top Link-->
	<div class="top-links hidden-xs">
		<div class="container">
			<div class="row">
					<div class="col-md-10">
						<div class="top-link">
							<span class="address-top"><i class="fa fa-map-marker "></i>C/ Física 13. 08038 BARCELONA.</span>
							<span class="call-top"><i class="fa fa-phone"></i> Tel 620 72 86 03 / 616 51 31 26</span>
						</div>
					</div>
					<div class="quick-access col-md-2 col-sm-2">								
						<div class="shopping-cart">
							<a href="#"><i class="fa fa-facebook"></i></a>
						</div>
						<div class="shopping-cart">
							<a href="#"><i class="fa fa-instagram"></i></a>
						</div>
						<div class="search">
							<a href="#"><i class="fa fa-search"></i></a>
							<form id="search" method="post" name="">
								<div class="search-box">
									<input class="top-search" type="text" placeholder="Enter your keywords" name="search" title="Search">						
									<input class="sub-search" type="image" src="<?= base_url() ?>theme/theme/images/search.png" alt="Submit">
								</div>
							</form>										
						</div>
					</div>
			</div>
		</div>
	</div>
	<!--End Top Link-->
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6 col-xs-8 logo">
				<a href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/theme/images/logo.png" alt=""/></a>
			</div>
			<div class="col-md-8 nav-container">
				<nav class="megamenu collapse navbar-collapse bs-navbar-collapse navbar-right mainnav col-md-10" role="navigation">
					<ul class="nav-menu">
						<li class="selected active"><a href="<?= base_url() ?>">Inicio</a></li>
						<li><a href="<?= base_url() ?>nosotros.html">Nosotros</a></li>
						<li>
							<a href="#">
								Servicios
							</a>
							<ul class="child-nav dropdown-nav">
								<li><a href="escuela_porteros.html">Escuela de porteros</a></li>
								<li><a href="campus.html">Campus de porteros</a></li>
								<li><a href="curso.html">Curso entrenadores</a></li>
								<li><a href="servicios_detalles.html">Campus internacionales</a></li>
							</ul>
						</li>
						<li><a href="<?= base_url() ?>blog">Noticias</a></li>
						<li><a href="<?= base_url() ?>colaboracion.html">Colaboración</a></li>
						<li><a href="<?= base_url() ?>multimedia.html">Multimedia</a></li>
						<li><a href="<?= base_url() ?>contacto.html">Contacto</a></li>
					</ul>
				</nav>							
			</div>							
			<button class="menu-button" id="open-button"></button>
		</div>
	</div>
</header>
<!--End Header-->