<section  class="page page-category header-option">
	<?php $this->load->view($this->theme.'_menu_mobile',array(),FALSE,'paginas'); ?>	
	<div class="content-wrapper">
		<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>	
		

		<!--Banner-->
				<section class="page-heading">
					<div class="title-slide">
						<div class="container">
								<div class="banner-content slide-container">									
									<div class="page-title">
										<h3>Our Blog</h3>
									</div>
								</div>
						</div>
					</div>
				</section>
				<!--End Banner-->
				<div class="page-content">					
					<!-- Breadcrumbs -->
					<div class="breadcrumbs">
						<div class="container">
							<div class="row">
								<div class="col-md-9">
									<ul>
										<li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
										<li><span>//</span></li>
										<li class="category-1"><a href="#">Blog</a></li>
										<li><span>//</span></li>
										<li class="category-2"><a href="#">Blog listing</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- End Breadcrumbs -->
					<!-- Main Content -->
					<div class="main-content our-blog">
						<div class="container">
							<div class="row">
								<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">																											
									<!--  Blog -->
									<div class="blog-page">
										<section class="blog-listing">
											<section class="blog-item">
												<div class="img-blog">
													<img src="<?= base_url() ?>theme/theme/images/bg-our-class-details.jpg" alt=""/>
												</div>
												<div class="blog-main">
													<div class="img-blog">
														<img src="<?= base_url() ?>theme/theme/images/img-pp.png" alt=""/>
													</div>
													<div class="blog-content">
														<div class="blog-header">
															<div class="blog-title-top">
																<a href="#">Success Stories</a>
															</div>
															<div class="blog-title">
																<a href="<?= base_url() ?>blog-detalle.html">Mayra Collins Lost 57 LBR In Just 4 Months</a>
															</div>
															<div class="blog-intro">
																<div class="created-by"> By <span>Mayra Collins</span></div>
																<div class="published"> On 5 October, 2014 </div>
																<div class="category-name">
																	in <a href="#">Success Stories</a>												
																</div>
															</div>
														</div>
														<div class="blog-text">
															<p>
																Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porttitor sapien ut odio luctus, non lobortis quam eleifend. Nunc venenatis 
																vestibulum nisl. Nullam eu enim feugiat, 
															</p>
														</div>
													</div>
												</div>
											</section>
											<section class="blog-item">
												<div class="img-blog">
													<img src="<?= base_url() ?>theme/theme/images/bg-blog.jpg" alt=""/>
												</div>
												<div class="blog-main">
													<div class="img-blog">
														<img src="<?= base_url() ?>theme/theme/images/img-pp.png" alt=""/>
													</div>
													<div class="blog-content">
														<div class="blog-header">
															<div class="blog-title-top">
																<a href="#">Success Stories</a>
															</div>
															<div class="blog-title">
																<a href="<?= base_url() ?>blog-detalle.html">Mayra Collins Lost 57 LBR In Just 4 Months</a>
															</div>
															<div class="blog-intro">
																<div class="created-by"> By <span>Mayra Collins</span></div>
																<div class="published"> On 5 October, 2014 </div>
																<div class="category-name">
																	in <a href="#">Success Stories</a>												
																</div>
															</div>
														</div>
														<div class="blog-text">
															<p>
																Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porttitor sapien ut odio luctus, non lobortis quam eleifend. Nunc venenatis 
																vestibulum nisl. Nullam eu enim feugiat,
															</p>
														</div>
													</div>
												</div>
											</section>
											<section class="blog-item">
												<div class="img-blog">
													<img src="<?= base_url() ?>theme/theme/images/bg-blog-1.jpg" alt=""/>
												</div>
												<div class="blog-main">
													<div class="img-blog">
														<img src="<?= base_url() ?>theme/theme/images/img-pp.png" alt=""/>
													</div>
													<div class="blog-content">
														<div class="blog-header">
															<div class="blog-title-top">
																<a href="#">Success Stories</a>
															</div>
															<div class="blog-title">
																<a href="<?= base_url() ?>blog-detalle.html">Mayra Collins Lost 57 LBR In Just 4 Months</a>
															</div>
															<div class="blog-intro">
																<div class="created-by"> By <span>Mayra Collins</span></div>
																<div class="published"> On 5 October, 2014 </div>
																<div class="category-name">
																	in <a href="#">Success Stories</a>												
																</div>
															</div>
														</div>
														<div class="blog-text">
															<p>
																Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porttitor sapien ut odio luctus, non lobortis quam eleifend. Nunc venenatis 
																vestibulum nisl. Nullam eu enim feugiat,
															</p>
														</div>
													</div>
												</div>
											</section>
											<section class="blog-item">
												<div class="img-blog">
													<img src="<?= base_url() ?>theme/theme/images/bg-blog-2.jpg" alt=""/>
												</div>
												<div class="blog-main">
													<div class="img-blog">
														<img src="<?= base_url() ?>theme/theme/images/img-pp.png" alt=""/>
													</div>
													<div class="blog-content">
														<div class="blog-header">
															<div class="blog-title-top">
																<a href="#">Success Stories</a>
															</div>
															<div class="blog-title">
																<a href="<?= base_url() ?>blog-detalle.html">Mayra Collins Lost 57 LBR In Just 4 Months</a>
															</div>
															<div class="blog-intro">
																<div class="created-by"> By <span>Mayra Collins</span></div>
																<div class="published"> On 5 October, 2014 </div>
																<div class="category-name">
																	in <a href="#">Success Stories</a>												
																</div>
															</div>
														</div>
														<div class="blog-text">
															<p>
																Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porttitor sapien ut odio luctus, non lobortis quam eleifend. Nunc venenatis 
																vestibulum nisl. Nullam eu enim feugiat,
															</p>
														</div>														
													</div>
												</div>
											</section>
											<section class="blog-item">
												<div class="img-blog">
													<img src="<?= base_url() ?>theme/theme/images/bg-our-class-details.jpg" alt=""/>
												</div>
												<div class="blog-main">
													<div class="img-blog">
														<img src="<?= base_url() ?>theme/theme/images/img-pp.png" alt=""/>
													</div>
													<div class="blog-content">
														<div class="blog-header">
															<div class="blog-title-top">
																<a href="#">Success Stories</a>
															</div>
															<div class="blog-title">
																<a href="<?= base_url() ?>blog-detalle.html">Mayra Collins Lost 57 LBR In Just 4 Months</a>
															</div>
															<div class="blog-intro">
																<div class="created-by"> By <span>Mayra Collins</span></div>
																<div class="published"> On 5 October, 2014 </div>
																<div class="category-name">
																	in <a href="#">Success Stories</a>												
																</div>
															</div>
														</div>
														<div class="blog-text">
															<p>
																Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porttitor sapien ut odio luctus, non lobortis quam eleifend. Nunc venenatis 
																vestibulum nisl. Nullam eu enim feugiat, 
															</p>
														</div>
													</div>
												</div>
											</section>
											<div class="pages">
												<ul>
													<li>
														<a class="previous i-previous" href="#" title="Next">
															<i class="fa fa-angle-left v-middle"></i>
														</a>
													</li>
													<li class="current"><a href="#">1</a></li>
													<li><a href="#">2</a></li>
													<li>
														<a class="next i-next" href="#" title="Next">
															<i class="fa fa-angle-right v-middle"></i>
														</a>
													</li>													
												</ul>
											</div>
										</section>
									</div>
										<!-- End Blog -->																	
								</div>		
								<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
									<div class="sidebar-left">
										<section class="menu-sidebar">
											<div class="title-menu">
												<h4>Blog categories</h4>
											</div>
											<ul class="nav-menu">
												<li><a href="#">Training blog</a></li>
												<li><a href="#">Street fitness</a></li>
												<li><a href="#">Sport</a></li>
												<li><a href="#">Boxing</a></li>
												<li><a href="#">Yoga</a></li>
												<li><a href="#">Success stories</a></li>
											</ul>
										</section>
										<section class="popular-event">
											<div class="popular-event-title">
												<h4>Popular Event</h4>
											</div>
											<div class="news-content">
												<ul>
													<li class="latest-news-item">
														<div class="news-item-inner">
															<div class="news-date">
																<div class="news-day">25</div>
																<div class="news-month">SEP</div>
															</div>
															<div class="news-info">
																<div class="news-title">
																	<a href="#">Repeating month event</a>
																</div>
																<div class="news-time">
																	<p>8:00 A.M- 10:00 P.M</p>				
																</div>
															</div>
														</div>
													</li>
													<li class="latest-news-item">
														<div class="news-item-inner">
															<div class="news-date">
																<div class="news-day">25</div>
																<div class="news-month">SEP</div>
															</div>
															<div class="news-info">
																<div class="news-title">
																	<a href="#">Cras commodo suscipit </a>
																</div>
																<div class="news-time">
																	<p>8:00 A.M- 10:00 P.M</p>				
																</div>
															</div>
														</div>
													</li>
													<li class="latest-news-item">
														<div class="news-item-inner">
															<div class="news-date">
																<div class="news-day">25</div>
																<div class="news-month">SEP</div>
															</div>
															<div class="news-info">
																<div class="news-title">
																	<a href="#">Nunc imperdiet libero sapien, ut faucibus mag</a>
																</div>
																<div class="news-time">
																	<p>8:00 A.M- 10:00 P.M</p>				
																</div>
															</div>
														</div>
													</li>
													<li class="latest-news-item">
														<div class="news-item-inner">
															<div class="news-date">
																<div class="news-day">25</div>
																<div class="news-month">SEP</div>
															</div>
															<div class="news-info">
																<div class="news-title">
																	<a href="#">Mauris id commodo dolor</a>
																</div>
																<div class="news-time">
																	<p>8:00 A.M- 10:00 P.M</p>				
																</div>
															</div>
														</div>
													</li>	
													<li class="latest-news-item">
														<div class="news-item-inner">
															<div class="news-date">
																<div class="news-day">25</div>
																<div class="news-month">SEP</div>
															</div>
															<div class="news-info">
																<div class="news-title">
																	<a href="#">Lorem ipsum dolor sit amet, consectetur</a>
																</div>
																<div class="news-time">
																	<p>8:00 A.M- 10:00 P.M</p>				
																</div>
															</div>
														</div>
													</li>	
												</ul>
											</div>										
										</section>
										<section class="archives">
											<div class="archives-title">
												<span>Archives</span>
											</div>
											<div class="archives-content">
												<div class="info-content">
													<a href="#"><span><i class="fa fa-calendar-o"></i>January 2014</span></a>												
												</div>
											</div>
											<div class="archives-content">
												<div class="info-content">
													<a href="#"><span><i class="fa fa-calendar-o"></i>August 2013</span></a>													
												</div>
											</div>
											<div class="archives-content">
												<div class="info-content">
													<a href="#"><span><i class="fa fa-calendar-o"></i>July 2013</span></a>													
												</div>
											</div>	
											<div class="archives-content">
												<div class="info-content">
													<a href="#"><span><i class="fa fa-calendar-o"></i>June 2013</span></a>													
												</div>
											</div>	
											<div class="archives-content">
												<div class="info-content">
													<a href="#"><span><i class="fa fa-calendar-o"></i>May 2013</span></a>													
												</div>
											</div>	
											<div class="archives-content">
												<div class="info-content">
													<a href="#"><span><i class="fa fa-calendar-o"></i>March 2013</span></a>											
												</div>
											</div>	
										</section>
										<section class="tags">									
											<div class="tags-title archives-title">
												<span>Tags</span>
											</div>
											<div class="tags-content">
												<div class="tags-blog">
													<a href="#">Doctors</a>
												</div>
												<div class="tags-blog">
													<a href="#">Lorem ispum</a>
												</div>
												<div class="tags-blog">
													<a href="#">fitness</a>
												</div>
												<div class="tags-blog">
													<a href="#">boxing trainers</a>
												</div>
												<div class="tags-blog">
													<a href="#">health</a>
												</div>
												<div class="tags-blog">
													<a href="#">foods</a>
												</div>
											</div>
										</section>
									</div>	
								</div>								
							</div>
						</div>
					</div>
					<!-- Main Content -->
				</div>
	</div>
</section>





<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>