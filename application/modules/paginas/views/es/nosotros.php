<section  class="page page-category header-option">
	<?php $this->load->view($this->theme.'_menu_mobile',array(),FALSE,'paginas'); ?>	
	<div class="content-wrapper">
		<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>	
		
		<!--Banner-->
				<section class="page-heading">
					<div class="title-slide">
						<div class="container">
								<div class="banner-content slide-container">									
									<div class="page-title">
										<h3>Nosotros</h3>
									</div>
								</div>
						</div>
					</div>
				</section>
				<!--End Banner-->
				<div class="page-content">					
					<!-- Breadcrumbs -->
					<div class="breadcrumbs">
						<div class="container">
							<div class="row">
								<div class="col-md-9">
									 <ul>
										 <li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
										 <li><span>//</span></li>
										 <li class="category-2">Nosotros</li>
									 </ul>
								</div>
							</div>
                        </div>		
					</div>
					<!-- End Breadcrumbs -->
					
					<!-- Menu About-->
					<div class="navbar-custom navbar menu-about">
						<div class="container">
							<div class="row">					
								<div class="nav-container">
									<nav class="collapse navbar-collapse navbar-main-collapse mainnav" role="navigation">
										<ul class="nav-menu nav navbar-nav">
											<li class="selected active"><a href="#classes">Por qué Impulse Sport</a></li>
											<li><a href="#history">Nuestra Historia</a></li>
											<li><a href="#our-team">Nosotros</a></li>
										</ul>
									</nav>							
								</div>
							</div>
						</div>
					</div>
					<!-- End Menu About-->
					
					<!-- Athlete Class -->
					<section id="classes" class="classes">						
						<div class="container">						
							<div class="classes-athlete">
								<div class="classes-inner choose">
									<div class="classes-content">									
										<div class="container">
											<div class="row">
												<div class="col-md-6 col-sm-12 col-xs-12">
													<div class="block-left">
														<div class="about-img">
															<img src="<?= base_url() ?>theme/theme/images/bg-about-us.png" alt=""/>
														</div>
														<div class="about-title">
															<h3>Por qué Impulse Sport</h3>
														</div>
														<div class="about-desc">
															<p>
															Impulse Sport es una empresa que nace exclusivamente para ayudar a los porteros de fútbol en todo aquello imprescindible para su rendimiento y crecimiento deportivo. También ayudamos a entrenadores y a diferentes instituciones (clubes y federaciones) a mejorar en la formación de sus porteros.
															</p>
														</div>
													</div>
												</div>
												<div class="col-md-6 col-sm-12 col-xs-12">
													<div class="block-right">
														<div class="block-right-info">
															<div class="about-info">
																<div class="icon-block">
																	<i class="fa fa-tablet"></i>
																</div>
																<div class="about-details">
																	<h4 class="about-details-title">CALIDAD</h4>
																	<p>
																	Apostamos por una enseñanza de última generación. Nos formamos a diario para poder ofrecer la mejor calidad en nuestras enseñanzas.
																	</p>
																</div>
															</div>
															<div class="about-info">
																<div class="icon-block">
																	<i class="fa fa-paint-brush"></i>
																</div>
																<div class="about-details">
																	<h4 class="about-details-title">profesionalidad</h4>
																	<p>
																	La portería es elemental en un equipo y por eso ponemos todos nuestros valores y conocimientos en formar a futuros porteros.
																	</p>
																</div>
															</div>
															<div class="about-info">
																<div class="icon-block">
																	<i class="fa fa-paper-plane"></i>
																</div>
																<div class="about-details">
																	<h4 class="about-details-title">SERIEDAD Y HONESTIDAD</h4>
																	<p>
																	Somos profesionales, serios y honestos. Cualquier duda, correción o queja para nosotros es un "algo" a explicar mejor o mejorar.
																	</p>
																</div>
															</div>
														</div>
													</div>
												</div>																																																		
											</div>
										</div>																				
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- End Athlete Class -->
					
					<!--Modern Equipment-->
					<section class="training modern">					
						<div class="wpb-wrapper">
							<!-- PARALLAX WINDOWS -->
							<div class="parallax-block-wrap-module auto-width">
								<div class="parallax-block bt-advance-parallax" id="parallax-block-modern">									
									<!-- PARALLAX CONTENT -->
									<div class="control-button">
										<div class="nav-wrap hidden">
											<div class="nav-wrap-in next">
												<span class="nav-next"></span>
											</div>
											<div class="nav-wrap-in prev">
												<span class="nav-prev"></span>
											</div>
										</div>
										<div class="button-wrap">
											<span class="button close-btn"></span>
										</div>
									</div>
									<div class="parallax-background">
										<!--Textured and color-->
										<img alt="" src="<?= base_url() ?>theme/theme/images/bg-position-3.1.jpg">
									</div>
									
									<div class="parallax-block-content default-pos">
										<div class="container">
											<div class="parallax-text parallax-gallery">
												<h1 class="training-title">HISTORIA</h1>
												<p class="join-us">de dónde venimos...</p>
												<!-- <span class="button open-btn"><span>Servicios</span></span> -->								
											</div>
										</div>
									</div>
									
									<div class="overlay-loading">
										<span class="loading"><img src="<?= base_url() ?>theme/theme/images/loading-black.gif" alt=""/></span>
									</div>    

									<!-- GALLERY CONTENT -->
									<div class="parallax-content-wrap">
										<div class="parallax-content hidden">
											<div class="parallax-content-in">
												<div class="defxault">
													<div class="parallax-col">
														<div class="parallax-row in-pos box1">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-1.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img class="image-show" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-large/parallax-1.jpg" alt=""/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-2.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-2.jpg" class="image-show" alt=""/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-3.jpg"/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-3.jpg" class="image-show"/>
																	</div>	
																</div>	
															</div>
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb w3 mr-top">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-4.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-4.jpg" class="image-show"/>
																	</div>
																</div>	
															</div>
															<div class="parallax-row-w3">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-5.jpg"/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-5.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-6.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-6.jpg" class="image-show"/>
																	</div>
																</div>	
															</div>
														</div>
														<div class="parallax-row in-pos box2">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-7.jpg"/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-7.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-8.jpg"/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-8.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-9.jpg"/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-9.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">	
																<div class="parallax-box-image">
																	<div class="thumb w3">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-10.jpg"/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-10.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
														</div>									
													</div>
													<div class="parallax-col">
														<div class="parallax-row in-pos box1">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-1.jpg"/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-1.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-2.jpg"/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-2.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-3.jpg"/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-3.jpg" class="image-show"/>
																	</div>
																</div>	
															</div>
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb w3 mr-top">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-4.jpg"/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-4.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
															<div class="parallax-row-w3">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-5.jpg"/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-5.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-6.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-6.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
														</div>
														<div class="parallax-row in-pos box2">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-7.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-7.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-8.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-8.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-9.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-9.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">	
																<div class="parallax-box-image">
																	<div class="thumb w3">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-10.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-10.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
														</div>									
													</div>
													<div class="parallax-col">
														<div class="parallax-row in-pos box1">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-1.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-1.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-2.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-2.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-3.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-3.jpg" class="image-show"/>
																	</div>	
																</div>	
															</div>
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb w3 mr-top">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-4.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-4.jpg" class="image-show"/>
																	</div>
																</div>	
															</div>
															<div class="parallax-row-w3">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-5.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-5.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-6.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-6.jpg" class="image-show"/>
																	</div>
																</div>	
															</div>
														</div>
														<div class="parallax-row in-pos box2">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-7.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-7.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-8.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-8.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-9.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-9.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">	
																<div class="parallax-box-image">
																	<div class="thumb w3">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-10.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-10.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
														</div>									
													</div>
													<div class="parallax-col">
														<div class="parallax-row in-pos box1">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-1.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-1.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-2.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-2.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-3.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-3.jpg" class="image-show"/>
																	</div>
																</div>	
															</div>
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb w3 mr-top">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-4.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-4.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
															<div class="parallax-row-w3">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-5.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-5.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-6.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-6.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
														</div>
														<div class="parallax-row in-pos box2">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-7.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-7.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-8.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-8.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-9.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-9.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">	
																<div class="parallax-box-image">
																	<div class="thumb w3">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-10.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-10.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
														</div>									
													</div>
													<div class="parallax-col">
														<div class="parallax-row in-pos box1">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-1.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-1.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-2.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-2.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-3.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-3.jpg" class="image-show"/>
																	</div>	
																</div>	
															</div>
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb w3 mr-top">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-4.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-4.jpg" class="image-show"/>
																	</div>
																</div>	
															</div>
															<div class="parallax-row-w3">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-5.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-5.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-6.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-6.jpg" class="image-show"/>
																	</div>
																</div>	
															</div>
														</div>
														<div class="parallax-row in-pos box2">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-7.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-7.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-8.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-8.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-9.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-9.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">	
																<div class="parallax-box-image">
																	<div class="thumb w3">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-10.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-10.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
														</div>									
													</div>
													<div class="parallax-col">
														<div class="parallax-row in-pos box1">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-1.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-1.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-2.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-2.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-3.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-3.jpg" class="image-show"/>
																	</div>
																</div>	
															</div>
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb w3 mr-top">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-4.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-4.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
															<div class="parallax-row-w3">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-5.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-5.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-6.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-6.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
														</div>
														<div class="parallax-row in-pos box2">
															<div class="parallax-row-w1">
																<div class="parallax-box-image">
																	<div class="thumb w2">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-7.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-7.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-8.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-8.jpg" class="image-show"/>
																	</div>
																</div>
																<div class="parallax-box-image">
																	<div class="thumb">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-9.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-9.jpg" class="image-show"/>
																	</div>
																</div>
															</div>	
															<div class="parallax-row-w2">	
																<div class="parallax-box-image">
																	<div class="thumb w3">
																		<img src="<?= base_url() ?>theme/theme/images/parallax-10.jpg" alt=""/>
																	</div>
																	<div class="show_box hidden">
																		<img alt="" src="<?= base_url() ?>theme/theme/images/parallax-large/parallax-10.jpg" class="image-show"/>
																	</div>
																</div>
															</div>
														</div>									
													</div>
												</div>	
											</div>
											<div class="content-show-large hidden">
												<div class="item-contain"></div>
												<div class="loading"><img src="<?= base_url() ?>theme/theme/images/loading-black.gif" alt=""/></div>
											</div>
										</div>
									</div>
									<!-- end GALLERY CONTENT -->				  
								</div>
							</div>
						</div>													
					</section>				
					<!--End Modern Equipment-->
					
					<!-- Our History -->
					<section id="history" class="our-team-page">				
						<div class="our-team">
							<div class="our-team-content">
								<div class="our-team-inner">							
									<div class="custom">
										<div class="our-team-tabs our-team-bottom our-team-fit tab-history" data-active="1">
											<div class="our-team-panes">
												<div class="our-team-pane our-team-clear">											
													<div class="our-history">
														<div class="container">
															<div class="row">
																<div class="col-md-6 col-sm-12 col-xs-12">
																	<img src="<?= base_url() ?>theme/theme/images/2008.jpg" alt="" style="width:100%;"/>
																</div>
																<div class="history-details col-md-6 col-sm-12 col-xs-12">
																	<h3>Formación</h3>
																	<div class="history_desc">
																		<li>1998: Alfredo inicia su formación y experiencia profesional como entrenador de futbol en el CIDARF (Centre d'Investigació i Desenvolupament d'Alt Rendiment en Futbol)</li>
																		<li>2000: Alfredo empieza su investigación y desarrollo de un método de entrenamiento específico de porteros de fútbol al CIDARF.</li>
																	<li>2002: Dirección y ejecución del campus de porteros Diadora a St Jordi de Cercs.</li>
																	<li>2004: Alfredo ficha por el CE Granollers como entrenador de porteros del 1.º equipo (3ª división nacional) y responsable de la formación de los porteros de todo el club.</li>
																	<li>2005: Alfredo ficha como entrenadores de porteros del equipo filial de la UDA Gramenet consiguiendo ascenso a 3ª nacional.</li>
																	<li>2006: Alfredo ficha por el CF Mollet como entrenador de porteros del 1r equipo (1ª Catalana) y responsable de la formación de porteros de todo el club.</li>
																	<li>2008: Glòria estudia y aprueba el curso de Representación de futbolistas de la RFEF.</li>
																	</div>
																	<div class="history-skill">
																		<ul>
																			
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="our-team-pane our-team-clear">											
													<div class="our-history">
														<div class="container">
															<div class="row">
																<div class="col-md-6 col-sm-12 col-xs-12">
																	<img src="<?= base_url() ?>theme/theme/images/2009.jpg" alt="" style="width:100%;"/>
																</div>
																<div class="history-details col-md-6 col-sm-12 col-xs-12">
																	<h3>Fundación y primeros Campus</h3>
																	<div class="history_desc">
																		<li>Glòria y Alfredo fundan la empresa Impulse Sport – Entrenamiento y representación de porteros de fútbol</li>
																	<li>En Julio se celebra el 1r campus de porteros Impulse Sport en St. Jordi de Cercs.</li>
																	<li>Se inicia la 1ª temporada de la Escuela de porteros Impulse Sport, con convenio de colaboración con la empresa Soccer Services y lo CF Can Vidalet.</li>
																	<li>Alfredo es nombrado profesor y encargado de desarrollar los contenidos de formación, en materia de entrenamiento de porteros, en los cursos de entrenadores de fútbol (niveles 1, 2 y Nacional) de la FCF.</li>
																	</div>
																	<div class="history-skill">
																		<ul>
																			
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="our-team-pane our-team-clear">											
													<div class="our-history">
														<div class="container">
															<div class="row">
																<div class="col-md-6 col-sm-12 col-xs-12">
																	<img src="<?= base_url() ?>theme/theme/images/2010.jpg" alt="" style="width:100%;"/>
																</div>
																<div class="history-details col-md-6 col-sm-12 col-xs-12">
																	<h3>Más desarrollo</h3>
																	<div class="history_desc">
																		Desarrollo de los contenidos de entrenamiento de porteros, para los cursos de monitor de futbol online, en colaboración con la empresa Soccer Services y Johan Cruyff Institute.
																	</div>
																	<div class="history-skill">
																		<ul>
																			
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="our-team-pane our-team-clear">											
													<div class="our-history">
														<div class="container">
															<div class="row">
																<div class="col-md-6 col-sm-12 col-xs-12">
																	<img src="<?= base_url() ?>theme/theme/images/2011.jpg" alt="" style="width:100%;"/>
																</div>
																<div class="history-details col-md-6 col-sm-12 col-xs-12">
																	<h3>Nueva sede... Ripoll</h3>
																	<div class="history_desc">
																		Los campus de verano cambian de sede a Ripoll, en colaboración con el Ayuntamiento de Ripoll.
																	</div>
																	<div class="history-skill">
																		<ul>
																			<li>Más equipamientos deportivos</li>
																			
																			<li>Entorno natural inigualable</li>
																			<li>Alojamiento con piscina, pistas deportivas muy cercano al campo de entrenamiento</li>
																			
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="our-team-pane our-team-clear">											
													<div class="our-history">
														<div class="container">
															<div class="row">
																<div class="col-md-6 col-sm-12 col-xs-12">
																	<img src="<?= base_url() ?>theme/theme/images/2012.jpg" alt="" style="width:100%;"/>
																</div>
																<div class="history-details col-md-6 col-sm-12 col-xs-12">
																	<h3>Expansión profesional</h3>
																	<div class="history_desc">
																		<li>Alfredo entra como responsable de la planificación y desarrollo de los campus de porteros en EEUU, en colaboración con la Oklahoma Soccer Association.
</li>
																			<li>Alfredo ficha como entrenador de porteros del CF Badalona en 2ºB.</li>
																			<li>En 2015 Alfredo entra como responsable de la planificación y desarrollo de los campus de porteros en Japón (Osaka, Tokyo, Yokohama, Kochi, Nara, …) en colaboración con la empresa Soccer Services.</li>
																	</div>
																	<div class="history-skill">
																		<ul>
																			
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="our-team-pane our-team-clear">											
													<div class="our-history">
														<div class="container">
															<div class="row">
																<div class="col-md-6 col-sm-12 col-xs-12">
																	<img src="<?= base_url() ?>theme/theme/images/2013.jpg" alt="" style="width:100%;"/>
																</div>
																<div class="history-details col-md-6 col-sm-12 col-xs-12">
																	<h3>Publicaciones</h3>
																	<div class="history_desc">
																		En 2015 Alfredo publica, como coautor, su 1r libro: <strong>“Las áreas de especialización y la dirección de equipos”</strong> dentro de la colección de libros de Sans Torrelles & Frattarola, C. del editorial MC Sports.
																		El año siguiente se publica el segundo libro, como coautor, de Albredo con Sans, A. & Frattarola, C. del libro: <strong>“La planificación y programación para un proceso formativo”</strong> del editorial MC Sports.
																	</div>
																	<div class="history-skill">
																		<ul>
																			
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="our-team-nav">									
												<span class="">1998-2008</span>
												<span class="">2008</span>
												<span class="our-team-current">2009</span>
												<span class="">2010</span>
												<span class="">2014</span>
												<span class="">2015</span>												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>						
					</section>
					<!-- End Our History -->
					
					<!-- Our Team -->
					<section id="our-team" class="our-team-page">				
						<div class="our-team">
							<div class="our-team-content">
								<div class="our-team-inner">							
									<div class="custom">
										<div class="our-team-tabs our-team-bottom our-team-fit content-our-team" data-active="1">
											<div class="our-team-panes">
												<div class="our-team-pane our-team-clear">
													<div class="our-team-img col-md-6 col-sm-4 col-xs-12">
														<img src="<?= base_url() ?>theme/theme/images/gloria.png" alt="" data-mce-src="<?= base_url() ?>theme/theme/images/img_tabBlock4.jpg"  style="width:100%" />
													</div>
													<div class="detail-our-team col-md-6 col-sm-8 col-xs-12">
														<div class="detail-our-team-inner">
															<div class="detail-our-team-desc">Gerente de Impulse Sport 
															y se encarga de realizar aquellas gestiones que son externas 
															a la dirección técnica que permitan ofrecer un buen servicio a nuestros clientes.
 															<br>
 															<br><b>FORMACIÓN ACADÉMICA Y PROFESIONAL</b>
															<br>• Licenciada en Administración y Dirección de Empresas (ESADE).
															<br>• Agente FIFA.
															<br>
 
															<br><b>EXPERIENCIA PROFESIONAL</b>
															<br>• Gerente Impulse Sport.
															<br>• Adjunta a gerencia de la empresa de fútbol Soccer Services.
															</div>
															
															<!-- 
<div class="detail-our-team-user">Gloria Homs</div>
															<div class="detail-our-team-pos">Gerente</div>
 -->
														</div>
													</div>
													
												</div>
												<div class="our-team-pane our-team-clear">
													<div class="our-team-img col-md-6 col-sm-4 col-xs-12">
														<img src="<?= base_url() ?>theme/theme/images/fidu.png" alt="" data-mce-src="<?= base_url() ?>theme/theme/images/img_tabBlock4_2.jpg" style="width:100%" />
													</div>
													<div class="detail-our-team col-md-6 col-sm-8 col-xs-12">
														<div class="detail-our-team-inner">
															<div class="detail-our-team-desc">Director técnico de Impulse Sport y el responsable de ayudar a nuestros clientes a conseguir sus objetivos. Tiene más de 15 años de experiencia en el entrenamiento del portero de futbol. En su currículum, destaca:
 															<br>
 															<br><b>FORMACIÓN ACADÉMICA Y PROFESIONAL</b>
															<br>• Licenciado en Ciencias de la Actividad Física y del Deporte (Blanquera- URLL).
															<br>• Diplomatura en Magisterio en Educación Física (Blanquera- URLL).
															<br>• Máster especialista en el entrenamiento del portero de fútbol (RFEF).
															<br>• Máster en Alto Rendimiento en Deportes de Equipo. Máster CEDE - F.C. Barcelona.
															<br>• Entrenador Nacional de Fútbol (UEFA Pro).
															<br><br><b>EXPERIENCIA PROFESIONAL</b>
															<br>• Director técnico Impulse Sport.
															<br>• Entrenador de porteros del FC Badalona – 2ª División B.
															<br>• Responsable del desarrollo de contenidos y profesor de la asignatura de Entrenamiento de porteros de fútbol para la Federació Catalana de Fútbol.
															<br>• Desarrollo de Cursos de Formación on-line para Entrenadores de Fútbol en colaboración Johan Cruyff  Institute & Federació  Catalana de Fútbol.
															<br>• Responsable del área de entrenamiento de porteros del Centro de Investigación y Desarrollo del Alto Rendimiento en Fútbol (CIDARF).
															<br>• Entrenador de porteros en diferentes equipos de Cataluña.
															<br>• Entrenador de porteros (grupo Alto Rendimiento) en colaboración con Tommy N’Kono en el Plan Marcet.
															<br>• Profesor invitado para impartir clases de Alto Rendimiento Deportivo y Entrenamiento de porteros de fútbol. Universitat de VIC.
															</div>
														</div>
													</div>
													
												</div>
											</div>
											<div class="our-team-nav">
												<span class="" style="width: 50%;">
													<span class='our-team-name'>Gerente</span>
													<span class='our-team-position'>Gloria Homs</span>
												</span>
												<span class="yoga-trainer" style="width: 50%;">
													<span class='our-team-name'>Alfredo Rodríguez</span>
													<span class='our-team-position'>Director Técnico</span>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- End Our Team -->
				</div>
			</div>
		
	</div>
</section>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
	<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/shortcode-frontend.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.mixitup.js"></script>	
	<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/dropdown.js"></script>
	
	
   