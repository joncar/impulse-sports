<section  class="page page-category header-option">
	<?php $this->load->view($this->theme.'_menu_mobile',array(),FALSE,'paginas'); ?>	
	<div class="content-wrapper">
		<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>	
		
		<!--Banner-->
				<section class="page-heading">
					<div class="title-slide">
						<div class="container">
							<div class="banner-content slide-container">									
								<div class="page-title">
									<h3>Contacto</h3>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--End Banner-->
				<div class="page-content">					
					<!-- Breadcrumbs -->
					<div class="breadcrumbs">
						<div class="container">
							<div class="row">
								<div class="col-md-9">
									<ul>
										<li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
										<li><span>//</span></li>
										<li class="category-2"><a href="#">Contacto</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- End Breadcrumbs -->
					<!-- Our Team -->
					<section id="our-team" class="our-team-page">				
						<div class="our-team-head">
							<div class="container">
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="headding">
											<div class="headding-title">
												<h4>Nuestros datos de contacto</h4>
												<div class="headding-bottom"></div>
											</div>
											<ul class="headding-content">
												<li>
													
													
												</li>
												<li>
													<div class="icon-headding">
														<i class="fa fa-globe"></i>
													</div>
													<div class="cont-headding">
														<h5>Email </h5>
														<a href="htpp://athlete.zorrothemes.com">info@impulse-sport.es</a>
													</div>
												</li>
												<li>
													<div class="icon-headding">
														<i class="fa fa-phone"></i>
													</div>
													<div class="cont-headding">
														<h5>Teléfono </h5>
														<p>0034 620 72 86 03 - 0034 616 51 31 26</p>
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div class="col-md-8 col-sm-6 col-xs-12">
										<div class="headding-title">
											<h4>We'd love to hear from you</h4>
											<div class="headding-bottom"></div>
										</div>
										<div class="headding-content">
											<p>Have questions about how Athlete  can help you streamline operations, improve performance and easily monitor the success of each department? Wondering 
											how the integrated dealer management system works, and how it can work for you?
											</p>
											<p>
												Give us a call at <span>1-234-567-890</span> or send us a message using the form below and we'll connect you with a dealer management software expert who can answer 
												your questions.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- End Our Team -->
					<!-- Contact Form -->
					<section class="contact-form">
						<div class="contact-submit">
							<div class="container">
								<div class="row">
									<div class="col-md-3 col-sm-12 col-xs-12">
										<div class="contact">
											<h4>Contact Form</h4>
											<div class="headding-bottom"></div>
											<form id="main-contact-form" class="main-contact-form row" name="contact-form" method="post">
												<div class="form-group col-md-12">
													<input type="text" name="name" class="control" required="required" placeholder="Your Name">
												</div>
												<div class="form-group col-md-12">
													<input type="email" name="email" class="control" required="required" placeholder="Your Email">
												</div>									
												<div class="form-group col-md-12">
													<textarea name="message" id="message" required="required" class="control" rows="8" placeholder="Your Message"></textarea>
												</div>                        
												<div class="form-group form-submit col-md-12">
													<button type="submit" name="submit" class="btn-submit">Send Message</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="contact-map">							
							<div class="map-frame-event">
								<input type="hidden" class="map-zoom" value="11" />
								<input type="hidden" class="map-lat" value="40.67" />
								<input type="hidden" class="map-lng" value="-73.94" />
								<input type="hidden" class="map-icon-title" value="Athlete!" />
								<input type="hidden" class="map-icon-img" value="<?= base_url() ?>theme/theme/images/pin.png" />
							</div>
						</div>					
					</section>
					<!-- End Contact Form -->
					<!-- Our Partners -->
					<section class="our-partners">
						<div class="container">
							<div class="row">
								<div class="headding-title">
									<h4>Our Partners</h4>
									<div class="headding-bottom"></div>
								</div>
								<div class="brand">
									<div class="col-md-2 col-sm-4 col-xs-12">
										<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-02.png" alt=""></div>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-12">
										<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-03.png" alt=""></div>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-12">
										<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-04.png" alt=""></div>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-12">
										<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-05.png" alt=""></div>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-12">
										<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-06.png" alt=""></div>
									</div>
									<div class="col-md-2 col-sm-4 col-xs-12">
										<div class="img-brand"><img src="<?= base_url() ?>theme/theme/images/logos/home-logo-02.png" alt=""></div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- End Our Partners -->
				</div>
		
	</div>
</section>





<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/home.map.js"></script>	