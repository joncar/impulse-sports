<!--Footer-->
<footer class="page-footer">
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12 infomation">
					<div class="copy-right">
						<div class="footer-right">
							<div class="line1">Copyright &copy; 2018<a href="#"> Impulse Sport</a></div>
							<div class="line2">Diseñada y desarrollada por Jordi Magaña</div>
						</div>
					</div>
					<div class="social_icon">
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-google-plus"></i></a>
						<a href="#"><i class="fa fa-youtube"></i></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 location">							
					<div class="footer-title">
						<h4>Contacto</h4>			
					</div>
					<div class="address">
						<p>Tel : +34 620 72 86 03 / +34 616 51 31 26<br>Email : info@impulsesport.es</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 send-mail">							
					<div class="footer-title">
						<h4>Solicitud de información</h4>			
					</div>
					<form name="" method="post"  id="send-mail">												
						<div class="info">Déjanos tu correo electrónico y contactaremos contigo lo antes posible</div>						
						<div class="email">
							<input type="text" title="E-mail" name="user[email]" class="inputbox" placeholder="Tu email" >
							<button class="button" title="Submit" type="submit"><i class="fa fa-arrow-right"></i></button>
						</div>
					</form>
				</div>						
			</div>
		</div>
	</section>
</footer>
<!--End Footer-->

<!--To Top-->
<div id="copyright">
	<div class="container">

		<div class="back-to-top"><a title="BACK_TO_TOP" href="#top"><i class="fa fa-chevron-up"></i></a></div>

		<div class="clrDiv"></div>
	</div>
</div>
<!--End To Top-->
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/plugins.js"></script>	
	<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/custom.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/main.js"></script> 