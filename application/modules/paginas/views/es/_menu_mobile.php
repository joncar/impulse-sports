<!--Menu Mobile-->
<div class="menu-wrap">
	<div class="main-menu">
		<h4 class="title-menu">Main menu</h4>
		<button class="close-button" id="close-button"><i class="fa fa-times"></i></button>
	</div>
	<ul class="nav-menu">
		<li class="selected active"><a href="<?= base_url() ?>">Inicio</a></li>
		<li><a href="<?= base_url() ?>nosotros.html">Nosotros</a></li>
		<li><a href="<?= base_url() ?>servicios.html">Servicios</a></li>
		<li><a href="<?= base_url() ?>blog">Noticias</a></li>
		<li><a href="<?= base_url() ?>colaboracion.html">Colaboración</a></li>
		<li><a href="<?= base_url() ?>multimedia.html">Multimedia</a></li>
		<li><a href="<?= base_url() ?>contacto.html">Contacto</a></li>
	</ul>
</div>
<!--Menu Mobile-->