<!-- //Footer -->
<div class="darkout-menu"></div>        
<!-- //Appointment Form -->
<!-- External JavaScripts -->
<script src="<?= base_url() ?>theme/theme/js/jquery.js"></script>
<script src="<?= base_url() ?>theme/theme/js/plugins/bootstrap.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/plugins/slick.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/plugins/jquery.form.js"></script>
<script src="<?= base_url() ?>theme/theme/js/plugins/jquery.validate.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/plugins/moment.js"></script>
<script src="<?= base_url() ?>theme/theme/js/plugins/bootstrap-datetimepicker.js"></script>
<script src="<?= base_url() ?>theme/theme/js/plugins/jquery.waypoints.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/plugins/jquery.countTo.js"></script>
<script src="<?= base_url() ?>theme/theme/js/plugins/jquery.magnific-popup.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/plugins/imagesloaded.pkgd.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/plugins/isotope.pkgd.min.js"></script>
<!-- Custom JavaScripts -->
<script src="<?= base_url() ?>theme/theme/js/custom.js"></script>
<script src="<?= base_url() ?>theme/theme/js/forms.js"></script>