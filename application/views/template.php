<!doctype html>
	<html lang="en">
		<title><?= empty($title) ? 'Monalco' : $title ?></title>
	  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
		<meta name="description" content="<?= empty($keywords) ?'': str_replace("\"","",strip_tags($description)) ?>" /> 	
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
		<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
		<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
		<script>var URL = '<?= base_url() ?>';</script>
	    
		<!-- Theme ---->
		<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/style.css" type="text/css">
		<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/bootstrap.css" type="text/css">
		<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/font-awesome.css" type="text/css">
		<!--[if !IE 9]><!-->
		<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/effect.css" type="text/css">
		<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/animation.css" type="text/css">
		<!--<![endif]-->
		<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/masterslider.css" type="text/css">
		<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/ms-fullscreen.css" type="text/css">
		<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/owl.carousel.css">
		<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/owl.transitions.css">
		<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/color.css" type="text/css">
		<!-- Social-feed css -->
		<link href="<?= base_url() ?>css/social.css" rel="stylesheet" type="text/css">  
	</head>

	<body id="page-top" class="" data-offset="90" data-target=".navigation" data-spy="scroll">	
		<div class="wrapper hide-main-content"> 
			<?php $this->load->view($view); ?>
		</div>
	</body>
</html>